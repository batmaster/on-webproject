<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title">Cart</h3>
    </div>
    <div class="panel-body">
        <table class="table">
            <tbody id="table-cart"></tbody>

            <tbody><tr>
                <td><a href="?page=order"><button type="button" class="btn btn-success" id="button-checkout" style="width: 100%">คิดเงิน</button></a></td>
                <td><button type="button" class="btn btn-danger" id="button-clear-cart" style="width: 100%">Clear</button></td>
            </tr>
        </tbody></table>
    </div>
</div>

<!-- Confirm Clar Cart -->
<div class="modal fade" id="clear-cart-confirm">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Clear Cart</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure to clear cart item(s)?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-danger" id="button-confirm-clear-cart">Clear</button>
			</div>
		</div>
	</div>
</div>

<script>

$(document).ready(function() {
    showCart();
});

$("#logout").click(function() {
    $.ajax({
        url: 'db.php',
        type: "POST",
        data: {
            "function": "logout",
        }
    }).done(function(response) {
        window.location = "?page=home";


    });
});

function addToCart(id) {
    <?php
    if (!isset($_COOKIE["username_member"]) || $_COOKIE["username_member"] == "")
    echo "$('#alert-signin').modal({show: true}); return";
    ?>

    $.ajax({
        url: 'db.php',
        type: "POST",
        // dataType: "json",
        data: {
            "function": "add_product_to_cart",
            "member_id": '<?php echo $_COOKIE["id_member"]; ?>',
            "product_id": id
        }
    }).done(function(response) {
        // console.log(response);
        showCart();
    });
}

function showCart() {
    $.ajax({
        url: 'db.php',
        type: "POST",
        dataType: "json",
        data: {
            "function": "show_cart",
            "member_id": '<?php echo $_COOKIE["id_member"]; ?>'
        }
    }).done(function(results) {
        console.log(results);
        // console.log($("#table-cart >tbody >tr").length);
        $("#table-cart").empty();
        var total = 0;
        if (results.length == 0) {
            $("#table-cart").append("<tr><td>ว่างเปล่า</td><td></td></tr>");
        }
        else {
            for (var i = 0; i < results.length; i++) {
                total += results[i].amount * results[i].unit_price;
                $("#table-cart").append("<tr><td>" + results[i].name.substring(0, 15) + "</td><td align=\"right\">" + results[i].amount + "</td></tr>");
            }
        }

        $("#table-cart").append("<tr><td colspan=2 align=\"right\">รวม " + total + " บาท</td></tr>");
    });
}


	$("#button-clear-cart").click(function() {
	    $("#clear-cart-confirm").modal({
		    show: true
		});
		$("#button-confirm-clear-cart").focus();
	});

	$("#button-confirm-clear-cart").click(function() {
		$.ajax({
			url: 'db.php',
			type: "POST",
			data: {
                "function": "clear_cart",
                "member_id": '<?php echo $_COOKIE["id_member"]; ?>'
			}
		}).done(function(results) {
            // console.log(results);
            // alert();
            showCart();
            $("#clear-cart-confirm").modal('hide');
		});
	});

</script>
