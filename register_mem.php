<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">สมัครสมาชิก</h3>
    </div>
    <div class="panel-body">
        <!--สมัครสมาชิก-->
        <br>
        <h1 align="center">สมัครสมาชิก</h1>

        <!--ชื่อ-->
        <div class="input-group">
            <span class="input-group-addon">ชื่อ</span>
            <input type="text" class="form-control" id="input-name">
        </div>
        <br>
        <!--นามสกุล-->
        <div class="input-group">
            <span class="input-group-addon">นามสกุล</span>
            <input type="text" class="form-control" id="input-lastname">
        </div>
        <br>
        <!--อีเมล-->
        <div class="input-group">
            <span class="input-group-addon">อีเมล</span>
            <input type="text" class="form-control" id="input-email">
        </div>
        <br>
        <!--รหัสผ่าน-->
        <div class="input-group">
            <span class="input-group-addon">รหัสผ่าน</span>
            <input type="password" class="form-control" id="input-password">
        </div>
        <br>
        <!--ยืนยันรหัสผ่าน-->
        <div class="input-group">
            <span class="input-group-addon">ยืนยันรหัสผ่าน</span>
            <input type="password" class="form-control" id="input-password1">
        </div>
        <br>
        <!--ที่อยู่-->
        <div class="input-group">
            <span class="input-group-addon">ที่อยู่1</span>
            <input type="text" class="form-control" id="input-address1">
        </div>
        <br>

        <div class="input-group">
            <span class="input-group-addon">ที่อยู่2</span>
            <input type="text" class="form-control" id="input-address2">
        </div>
        <br>

        <div class="input-group">
            <span class="input-group-addon">เขต/อำเภอ</span>
            <input type="text" class="form-control" id="input-district">
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon">จังหวัด</span>
            <input type="text" class="form-control" id="input-province">
        </div>
        <br>
        <div class="input-group">
            <span class="input-group-addon">รหัสไปรษณีย์</span>
            <input type="text" class="form-control" id="input-zip">
        </div>
        <br>

        <!--เบอร์โทร-->
        <div class="input-group">
            <span class="input-group-addon">เบอร์โทร</span>
            <input type="text" class="form-control" id="input-tel">
        </div>
        <br>
        <button type="button" class="btn btn-success navbar-btn" id="button-submit">บันทึก</button>
        <button type="button" class="btn btn-danger navbar-btn" id="button-cancel">ยกเลิก</button>
    </div>
</div>

<script type="text/javascript">
// ปุ่ม id="button-submit"
$("#button-submit").click(function() {
    var id = $("#input-id").val();
    var name = $("#input-name").val();
    var lastname = $("#input-lastname").val();
    var email = $("#input-email").val();
    var password = $("#input-password").val();
    var password1 = $("#input-password1").val();
    var address1 = $("#input-address1").val();
    var address2 = $("#input-address2").val();
    var district = $("#input-district").val();
    var province = $("#input-province").val();
    var zip = $("#input-zip").val();
    var tel = $("#input-tel").val();

if (password == password1) {
    $.ajax({
        url: 'db.php',
        type: "POST",
        // dataType: "json",
        data: {
            "function": "add_member",
            "name": name,
            "lastname":lastname,
            "email":email,
            "password":password,
            "address1":address1,
            "address2":address2,
            "district":district,
            "province":province,
            "zip":zip,
            "tel":tel,
        }
    }).done(function(response) {
        location.reload();
    });
}
else {
    alert("รหัสไม่ตรงกัน")
}


});

$("#button-cancel").click(function() {
    location.reload();
});
</script>
