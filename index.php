<?php
    require 'db_connection.php';
?>

<html lang="en">
<head>
    <script src="js/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <script src="../js/collapse.js"></script>
    <script src="../js/moment.js"></script>
    <script src="../js/transition.js"></script>
    <script src="../js/bootstrap-datepicker.th.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.min.js"></script>

    <meta charset="UTF-8">
    <title>สากลโมบาย.COM</title>
</head>
<body>


    <div class="container">
<div><img width="100%" src="images/es.jpg" border="1" hspace="0"></div>
<nav class="navbar navbar-default">
  <div class="container-fluid">
      <div class="collapse navbar-collapse">



    <!--แถบบนสุด-->
    <ul class="nav nav-pills nav-justified">
        <li role="presentation"<?php if ($_GET["page"] == "home") echo "class=\"active\""; ?> ><a href="index.php">หน้าหลัก</a></li>
        <li role="presentation" <?php if ($_GET["page"] == "show") echo "class=\"active\""; ?> ><a href="?page=show">สินค้าทั้งหมด</a></li>
        <li role="presentation"<?php if ($_GET["page"] == "ordering") echo "class=\"active\""; ?> ><a href="?page=ordering">วิธีสั่งซื้อ</a></li>
        <li role="presentation"<?php if ($_GET["page"] == "pay") echo "class=\"active\""; ?> ><a href="?page=pay">วิธีชำระเงิน</a></li>
        <li role="presentation"<?php if ($_GET["page"] == "delivery") echo "class=\"active\""; ?> ><a href="?page=delivery">วิธีขนส่ง</a></li>

        <?php
        if (isset($_COOKIE["username_member"]) && $_COOKIE["username_member"] != "") {
            echo "<li role=\"presentation\""; if ($_GET["page"] == "profile" || $_GET["page"] == "edit" || $_GET["page"] == "ordered" || $_GET["page"] == "sale" || $_GET["page"] == "payment") echo "class=\"active\""; echo "><a href=\"?page=profile\">ข้อมูลส่วนตัว</a></li>";
        }
        ?>

        <li role="presentation"<?php if ($_GET["page"] == "contact") echo "class=\"active\""; ?> ><a href="?page=contact">ติดต่อเรา</a></li>
        <?php
        if (isset($_COOKIE["username_member"]) && $_COOKIE["username_member"] != "") {
            echo "<li role=\"presentation\"><a href=\"#\" id=\"logout\">ออกจากระบบ</a></li>";
        }
        ?>
    </ul>
</div>
</div>
</nav>
        <div class="row">
            <div class="col-xs-2">
                <div class="dropdown">
                    <button style="width: 100%" class="btn btn-info dropdown-toggle" type="button" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-brand="<?php if (isset($_GET["brand"]) && $_GET["brand"] != "all") echo $_GET["brand"]; else echo "ทั้งหมด"; ?>">
                        <?php if (isset($_GET["brand"]) && $_GET["brand"] != "all") echo $_GET["brand"]; else echo "ทั้งหมด"; ?><span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownSearch" id="dropdown-search">
                        <li><a href="#">ทั้งหมด</a></li>
                        <li><a href="#">IPhone</a></li>
                        <li><a href="#">Samsung</a></li>
                        <li><a href="#">AIS LAVA</a></li>
                        <li><a href="#">Zenfone</a></li>
                        <li><a href="#">Vivo</a></li>
                        <li><a href="#">OPPO</a></li>
                        <li><a href="#">HTC</a></li>
                        <li><a href="#">Wiko</a></li>
                        <li><a href="#">Acer</a></li>
                        <li><a href="#">Huawei</a></li>
                        <li><a href="#">Sony</a></li>
                        <li><a href="#">Lenovo</a></li>
                        <li><a href="#">LG</a></li>
                        <li><a href="#">i-mobile</a></li>
                        <li><a href="#">อื่นๆ</a></li>
                    </ul>
                </div>
                <br>
            </div>

            <div class="col-xs-10">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" id="search-box" value="<?php if (isset($_GET["text"])) echo $_GET["text"]; else echo ""; ?>">
                    <span class="input-group-btn">
                        <button id="search-button" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </span>


                </div>

            </div>

        </div>


        <br>

        <!--แถบที่2-->

        <div class="row">

            <div class="col-xs-9">
                        <?php
                        if ($_GET["page"] == "detail")
                        include "detail.php";
                        else if ($_GET["page"] == "register_mem")
                        include "register_mem.php";
                        else if ($_GET["page"] == "order")
                        include "order.php";
                        else if ($_GET["page"] == "cart")
                        include "cart_file.php";
                        else if ($_GET["page"] == "payment")
                        include "payment.php";
                        else if ($_GET["page"] == "profile")
                        include "profile.php";
                        else if ($_GET["page"] == "edit")
                        include "edit.php";
                        else if ($_GET["page"] == "show")
                        include "show.php";
                        else if ($_GET["page"] == "contact")
                        include "contact.php";
                        else if ($_GET["page"] == "delivery")
                        include "delivery.php";
                        else if ($_GET["page"] == "payment1")
                        include "payment1.php";
                        else if ($_GET["page"] == "ordering")
                        include "ordering.php";
                        else if ($_GET["page"] == "ordered")
                        include "ordered.php";
                        else if ($_GET["page"] == "pay")
                        include "pay.php";
                        else if ($_GET["page"] == "first")
                        include "first.php";
                        else if ($_GET["page"] == "report")
                        include "report.php";
                        else if ($_GET["page"] == "sale")
                        include "sale.php";
                        else
                        include "home.php";

                        ?>
                </div>
                <div class="col-xs-3">
                    <?php
                    if (!isset($_COOKIE["username_member"]) || $_COOKIE["username_member"] == "")
                    include "login_file.php";
                    else
                    include "cart_file.php";
                    ?>



                </div>
            </div>


        <div class="modal fade" id="alert-signin">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Login</h4>
                    </div>
                    <div class="modal-body">
                        <p>กรุณา login หรือ สมัครสมาชิก ก่อน</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">ตกลง</button>

                    </div>
                </div>
            </div>
        </div>
</div>


    </body>
    </html>
    <script>
        $("#logout").click(function() {
            $.ajax({
                url: 'db.php',
                type: "POST",
                data: {
                    "function": "logout"
                }
            }).done(function(response) {
                location.reload();
                console.log(response)
            });
        });

        $(document).ready(function(){
            $('.dropdown-toggle').dropdown();
        });

        $("#dropdown-search li a").click(function(e) {
          var selText = $(this).text();
          $('#dropdownSearch').html(selText+'<span class="caret"></span>');
          $('#dropdownSearch').attr("data-brand", selText);
          e.preventDefault();
        });

        $("#search-button").click(function() {
            var brand = $('#dropdownSearch').attr("data-brand");
            if (brand == "ทั้งหมด") {
                brand = "all";
            }
            var text = $("#search-box").val();

            window.location.href = "?page=show&brand=" + brand + "&text=" + text;
        });
    </script>
