-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jun 25, 2016 at 12:15 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `mobile`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `brand`
-- 

CREATE TABLE `brand` (
  `brand_id` int(10) NOT NULL auto_increment,
  `brand_name` varchar(30) NOT NULL,
  PRIMARY KEY  (`brand_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `brand`
-- 

INSERT INTO `brand` VALUES (1, 'i-phone');
INSERT INTO `brand` VALUES (2, 'samsung');
INSERT INTO `brand` VALUES (3, 'ais lava');
INSERT INTO `brand` VALUES (4, 'zenfone');
INSERT INTO `brand` VALUES (5, 'vivo');
INSERT INTO `brand` VALUES (6, 'oppo');
INSERT INTO `brand` VALUES (7, 'htc');
INSERT INTO `brand` VALUES (8, 'wiko');
INSERT INTO `brand` VALUES (9, 'acer');
INSERT INTO `brand` VALUES (10, 'huawei');
INSERT INTO `brand` VALUES (11, 'sony');
INSERT INTO `brand` VALUES (12, 'lenovo');
INSERT INTO `brand` VALUES (13, 'lg');
INSERT INTO `brand` VALUES (14, 'i-mobile');

-- --------------------------------------------------------

-- 
-- Table structure for table `cart`
-- 

CREATE TABLE `cart` (
  `cart_id` int(10) NOT NULL auto_increment,
  `member_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY  (`cart_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=96 ;

-- 
-- Dumping data for table `cart`
-- 

INSERT INTO `cart` VALUES (1, 1, 10, '4', '-1');
INSERT INTO `cart` VALUES (2, 1, 5, '6', '-1');
INSERT INTO `cart` VALUES (3, 1, 2, '64', '-1');
INSERT INTO `cart` VALUES (4, 1, 3, '71', '-1');
INSERT INTO `cart` VALUES (5, 1, 4, '7', '-1');
INSERT INTO `cart` VALUES (6, 1, 3, '38', '-1');
INSERT INTO `cart` VALUES (7, 1, 2, '43', '-1');
INSERT INTO `cart` VALUES (8, 1, 5, '4', '-1');
INSERT INTO `cart` VALUES (9, 1, 10, '2', '-1');
INSERT INTO `cart` VALUES (10, 1, 3, '32', '-1');
INSERT INTO `cart` VALUES (11, 1, 3, '23', '-1');
INSERT INTO `cart` VALUES (12, 1, 1, '34', '-1');
INSERT INTO `cart` VALUES (13, 1, 2, '35', '-1');
INSERT INTO `cart` VALUES (14, 1, 1, '31', '-1');
INSERT INTO `cart` VALUES (15, 1, 2, '21', '-1');
INSERT INTO `cart` VALUES (16, 1, 3, '23', '-1');
INSERT INTO `cart` VALUES (17, 1, 1, '15', '-1');
INSERT INTO `cart` VALUES (18, 1, 2, '20', '-1');
INSERT INTO `cart` VALUES (19, 1, 1, '15', '-1');
INSERT INTO `cart` VALUES (20, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (21, 1, 3, '22', '-1');
INSERT INTO `cart` VALUES (22, 1, 1, '15', '-1');
INSERT INTO `cart` VALUES (23, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (24, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (25, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (26, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (27, 1, 3, '22', '-1');
INSERT INTO `cart` VALUES (28, 1, 10, '1', '-1');
INSERT INTO `cart` VALUES (29, 1, 5, '1', '-1');
INSERT INTO `cart` VALUES (30, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (31, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (32, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (33, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (34, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (35, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (36, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (37, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (38, 1, 3, '22', '-1');
INSERT INTO `cart` VALUES (39, 1, 12, '1', '-1');
INSERT INTO `cart` VALUES (40, 1, 23, '1', '-1');
INSERT INTO `cart` VALUES (41, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (42, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (43, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (44, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (45, 1, 3, '22', '-1');
INSERT INTO `cart` VALUES (46, 1, 2, '19', '-1');
INSERT INTO `cart` VALUES (47, 1, 3, '22', '-1');
INSERT INTO `cart` VALUES (48, 1, 12, '1', '-1');
INSERT INTO `cart` VALUES (49, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (50, 1, 5, '1', '-1');
INSERT INTO `cart` VALUES (51, 1, 0, '1', '-1');
INSERT INTO `cart` VALUES (52, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (53, 1, 2, '17', '-1');
INSERT INTO `cart` VALUES (54, 1, 3, '21', '-1');
INSERT INTO `cart` VALUES (55, 1, 5, '1', '-1');
INSERT INTO `cart` VALUES (56, 1, 12, '1', '-1');
INSERT INTO `cart` VALUES (57, 1, 10, '1', '-1');
INSERT INTO `cart` VALUES (58, 1, 2, '17', '-1');
INSERT INTO `cart` VALUES (59, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (60, 1, 3, '21', '-1');
INSERT INTO `cart` VALUES (61, 1, 10, '1', '-1');
INSERT INTO `cart` VALUES (62, 1, 2, '17', '-1');
INSERT INTO `cart` VALUES (63, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (64, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (65, 1, 12, '1', '-1');
INSERT INTO `cart` VALUES (66, 1, 23, '1', '-1');
INSERT INTO `cart` VALUES (67, 1, 0, '1', '-1');
INSERT INTO `cart` VALUES (68, 1, 10, '1', '-1');
INSERT INTO `cart` VALUES (69, 1, 2, '17', '-1');
INSERT INTO `cart` VALUES (70, 1, 2, '17', '-1');
INSERT INTO `cart` VALUES (71, 1, 3, '21', '-1');
INSERT INTO `cart` VALUES (72, 1, 1, '14', '-1');
INSERT INTO `cart` VALUES (73, 1, 1, '13', '-1');
INSERT INTO `cart` VALUES (74, 1, 2, '16', '-1');
INSERT INTO `cart` VALUES (75, 1, 3, '21', '-1');
INSERT INTO `cart` VALUES (76, 1, 1, '11', '-1');
INSERT INTO `cart` VALUES (77, 1, 2, '14', '-1');
INSERT INTO `cart` VALUES (78, 1, 1, '10', '-1');
INSERT INTO `cart` VALUES (79, 1, 2, '14', '-1');
INSERT INTO `cart` VALUES (80, 1, 1, '7', '-1');
INSERT INTO `cart` VALUES (81, 1, 2, '12', '-1');
INSERT INTO `cart` VALUES (82, 1, 3, '21', '-1');
INSERT INTO `cart` VALUES (83, 1, 4, '6', '-1');
INSERT INTO `cart` VALUES (84, 1, 3, '4', '-1');
INSERT INTO `cart` VALUES (85, 1, 4, '5', '-1');
INSERT INTO `cart` VALUES (86, 1, 2, '3', '-1');
INSERT INTO `cart` VALUES (87, 1, 4, '3', '-1');
INSERT INTO `cart` VALUES (88, 1, 3, '3', '-1');
INSERT INTO `cart` VALUES (89, 1, 1, '1', '-1');
INSERT INTO `cart` VALUES (90, 1, 2, '2', '-1');
INSERT INTO `cart` VALUES (91, 1, 4, '2', '-1');
INSERT INTO `cart` VALUES (92, 1, 3, '2', '-1');
INSERT INTO `cart` VALUES (93, 1, 2, '1', '0');
INSERT INTO `cart` VALUES (94, 1, 3, '1', '0');
INSERT INTO `cart` VALUES (95, 1, 1, '1', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `dealer`
-- 

CREATE TABLE `dealer` (
  `dealer_id` int(10) NOT NULL auto_increment,
  `dealer_name` varchar(50) NOT NULL,
  `dealer_company` varchar(50) NOT NULL,
  `dealer_tel` varchar(13) NOT NULL,
  `employer_id` varchar(10) NOT NULL,
  `dealer_status` varchar(15) NOT NULL,
  PRIMARY KEY  (`dealer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `dealer`
-- 

INSERT INTO `dealer` VALUES (1, 'name', '$dealer_company', '$dealer_tel', 'employer', 'status');
INSERT INTO `dealer` VALUES (2, 'see', 'sung', '08755453', 't', 'u');
INSERT INTO `dealer` VALUES (3, 'sam', 'sung', '08755453', 't', 'u');
INSERT INTO `dealer` VALUES (4, 'lk', 'jl', '09', 'nj', '''o');

-- --------------------------------------------------------

-- 
-- Table structure for table `employee`
-- 

CREATE TABLE `employee` (
  `employer_id` int(10) NOT NULL auto_increment,
  `employer_name` varchar(20) NOT NULL,
  `employer_lastname` varchar(20) NOT NULL,
  `employer_password` varchar(10) NOT NULL,
  `employer_address` varchar(40) NOT NULL,
  `employer_idcard` varchar(13) NOT NULL,
  `employer_born` varchar(10) NOT NULL,
  `employer_sex` varchar(5) NOT NULL,
  `employer_salary` varchar(7) NOT NULL,
  `employer_tel` varchar(12) NOT NULL,
  PRIMARY KEY  (`employer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

-- 
-- Dumping data for table `employee`
-- 

INSERT INTO `employee` VALUES (1, 'op', 'oppp', '91', 'tt', '23', 'gr', '', '34', '0989900');
INSERT INTO `employee` VALUES (33, 'df', 'yu', 'fo', 'jj', 'jh', 'jghgh', 'gfhgf', 'jhgj', 'chjh');
INSERT INTO `employee` VALUES (34, 'gh', 'hg', '23', 'df', '678', '64', 'bh', '89', '9');
INSERT INTO `employee` VALUES (67, 'fg', 'hg', 'uo', 'op', '56', '67', 'bn', '456', '8999');
INSERT INTO `employee` VALUES (56, 'uoo', 'pop', '567', 'we', 'jk', '89', '7', '87', '00999');
INSERT INTO `employee` VALUES (68, 'ouu', 'you', 'nbn', 'op', '686', 'hhj', 'jhjh', 'gfg', 'opi');
INSERT INTO `employee` VALUES (69, 'ccvd', 'sds', 's', '', '', '', '', '', '');
INSERT INTO `employee` VALUES (32, 'drters', 'df', 'er', 'dsf', '', '', '', '', '');
INSERT INTO `employee` VALUES (70, 'tt', 'tt', 'tt', 'tt', '56', '4tg', '55gg', '55', '56');
INSERT INTO `employee` VALUES (71, 'b', 'b', 'b', 'b', 'b', 'b', 'b', '1000069', '100006949333');
INSERT INTO `employee` VALUES (4, 'ff', 'ff', 'ff', 'ff', '22333', '3d', 'dd', '32', '21');
INSERT INTO `employee` VALUES (90, 'u', 'h', '6', 'h', '9', '9', 'h', '9', '9');
INSERT INTO `employee` VALUES (44, 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r');

-- --------------------------------------------------------

-- 
-- Table structure for table `inform_pay`
-- 

CREATE TABLE `inform_pay` (
  `form_id` int(10) NOT NULL auto_increment,
  `pay_id` int(12) NOT NULL,
  `account_name` varchar(40) NOT NULL,
  `branch` varchar(40) NOT NULL,
  `account_number` varchar(10) NOT NULL,
  `date` varchar(20) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `transfer` varchar(15) NOT NULL,
  `cart_id` int(10) NOT NULL,
  `note` varchar(100) NOT NULL,
  PRIMARY KEY  (`form_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- 
-- Dumping data for table `inform_pay`
-- 

INSERT INTO `inform_pay` VALUES (1, 0, 'g', 'h', '', '2016-04-01 22:10:01', 'l', 'z', 0, 'c');
INSERT INTO `inform_pay` VALUES (2, 0, 'g', 'h', '', '2016-04-01 22:10:11', 'l', 'z', 0, 'c');
INSERT INTO `inform_pay` VALUES (3, 0, 'y', 'u', 'u', '2016-04-01 22:12:52', 'o', 'o', 0, 't');
INSERT INTO `inform_pay` VALUES (4, 0, 'e', 'e', 'e', '2016-04-01 22:13:47', 'e', 'e', 0, 'e');
INSERT INTO `inform_pay` VALUES (5, 0, '', '', '', '2016-04-01 22:58:17', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (6, 0, '', '', '', '2016-04-01 23:12:38', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (7, 0, '', '', '', '2016-04-17 12:45:39', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (8, 0, '', '', '', '2016-04-17 12:45:46', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (9, 0, '', '', '', '2016-04-20 00:50:09', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (10, 0, '', '', '', '2016-04-23 00:04:49', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (11, 0, '', '', '', '2016-04-23 09:05:23', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (12, 0, '', '', '', '2016-04-25 09:11:15', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (13, 0, '', '', '', '2016-04-25 09:11:23', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (14, 0, '', '', '', '2016-05-05 23:41:35', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (15, 0, '', '', '', '2016-05-16 20:34:34', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (16, 0, '', '', '', '2016-06-08 21:08:21', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (17, 0, '', '', '', '2016-06-18 13:17:42', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (18, 0, '', '', '', '2016-06-20 07:49:28', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (19, 0, '', '', '', '2016-06-20 07:51:17', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (20, 0, '', '', '', '2016-06-20 20:30:48', '', '', 0, '');
INSERT INTO `inform_pay` VALUES (21, 0, '', '', '', '2016-06-20 20:31:02', '', '', 0, '');

-- --------------------------------------------------------

-- 
-- Table structure for table `member`
-- 

CREATE TABLE `member` (
  `member_id` int(10) NOT NULL auto_increment,
  `member_name` varchar(20) NOT NULL,
  `member_lastname` varchar(20) NOT NULL,
  `member_email` varchar(30) NOT NULL,
  `member_password` varchar(10) NOT NULL,
  `member_number` varchar(5) NOT NULL,
  `member_tel` varchar(12) NOT NULL,
  `member_mu` varchar(5) NOT NULL,
  `member_road` varchar(30) NOT NULL,
  `memner_district` varchar(30) NOT NULL,
  `member_province` varchar(30) NOT NULL,
  `member_country` varchar(30) NOT NULL,
  `member_zip` varchar(5) NOT NULL,
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `member`
-- 

INSERT INTO `member` VALUES (1, 'new', 'newl', 'ss', 'asss', '9/2', '12', '9', 'suahueng', 'chianyai', 'nrt', 'th', '80190');
INSERT INTO `member` VALUES (2, 'gfdg', 'dgf', 'fhg', 'we', 'rt', '45', '', '', '', '', '', '');
INSERT INTO `member` VALUES (3, 'jkk', 'ioo', 'pjjf', '090', 'jgh', '9885324', '', '', '', '', '', '');
INSERT INTO `member` VALUES (4, 'lgdkg', 'fkelf', 'op', 'tpro', 'plr.', '896678', '', '', '', '', '', '');
INSERT INTO `member` VALUES (5, 'ewl,d', 'ofkdl;a', 'sdiklw', 'kjsdl;', 'djksl', '97896789', '', '', '', '', '', '');
INSERT INTO `member` VALUES (6, 'n', 'n', 'n', 'n', '', '5', '4', 'n', 'n', 'n', 'n', '3');

-- --------------------------------------------------------

-- 
-- Table structure for table `order`
-- 

CREATE TABLE `order` (
  `order_id` int(10) NOT NULL auto_increment,
  `product_id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `product_name` varchar(40) NOT NULL,
  `order_amount` varchar(5) NOT NULL,
  `order_date` varchar(12) NOT NULL,
  `employer_id` int(10) NOT NULL,
  `dealer_id` int(10) NOT NULL,
  PRIMARY KEY  (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `order`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `product_id` int(10) NOT NULL auto_increment,
  `product_name` varchar(40) NOT NULL,
  `product_color` varchar(20) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `product_other` varchar(50) NOT NULL,
  `product_cost` varchar(10) NOT NULL,
  `product_price` varchar(10) NOT NULL,
  `product_image` varchar(20) NOT NULL,
  `product_date` varchar(10) NOT NULL,
  `product_weight` varchar(10) NOT NULL,
  `product_notes` varchar(10) NOT NULL,
  `dealer_id` int(10) NOT NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (1, '4s', 'ทอง', 0, 'ram 2.0  gb  rom 1.0 gb    camera 8ล้านพิกเซล', '3789', '5780', 'u', '2016-06-18', '0.5', '', 2);
INSERT INTO `product` VALUES (2, 'galaxy j7', 'ทอง', 0, 'erefdf', '7990', '8990', '', '2016-06-18', '0.7', '', 1);
INSERT INTO `product` VALUES (3, 'win', 'ขาว', 0, 'jhlkj;', '4890', '5689', 'piop', '2016-06-19', '0.7', '', 0);
INSERT INTO `product` VALUES (4, 'a33w', 'while', 0, 'camera 8 pixel', '5690', '7890', 'fdf', '2016-06-19', '0.6', '', 5);

-- --------------------------------------------------------

-- 
-- Table structure for table `store`
-- 

CREATE TABLE `store` (
  `store_id` int(10) NOT NULL auto_increment,
  `product_id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `product_name` varchar(40) NOT NULL,
  `store_amount` varchar(5) NOT NULL,
  PRIMARY KEY  (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `store`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` varchar(10) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  `surename` varchar(40) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` VALUES ('$id', '$username', '$password', '$name', '$surename');
INSERT INTO `user` VALUES ('tr', 'tr', '45', 'tr', 'tr');
