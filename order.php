<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">สรุปการสั่งซื้อ</h3>
    </div>
    <div class="panel-body">
        <div>
            <table class="table table-striped table-bordered" id="id">
                <tr>
                    <th>#</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>ราคา</th>
                    <th>จำนวน</th>
                    <th>ราคารวม</th>
                </tr>
                <?php

                $member_id = $_COOKIE["id_member"];

                $sql = "SELECT p.id, p.name, p.price, c.amount, p.price * c.amount total FROM product p, cart c WHERE c.member_id = $member_id AND status = 0 AND p.id = c.product_id";
                //echo $sql;
                $result = mysql_query($sql);

                $i = 0;
                $total = 0;
                while ($r = mysql_fetch_assoc($result)) {
                    $i++;
                    $id = $r["id"];
                    $name= $r["name"];
                    $price=$r["price"];
                    $amount=$r["amount"];
                    $total_unit=$r["total"];
                    $total += $total_unit;

                    ?>
                    <tr>
                        <th><?php echo $i; ?></th>
                        <td><?php echo $id; ?></td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $price; ?></td>
                        <td><?php echo $amount; ?></td>
                        <td><?php echo $total_unit; ?></td>
                    </tr>
                    <?php
                }
                echo "<tr><th colspan=5>รวม</th>";
                echo "<th>$total</th></tr>";
                ?>
            </table>
        </div>
    </div>
</div>
<br>

<?php
$sql = "SELECT * FROM member WHERE id = $member_id";
$result = mysql_query($sql);

$rows = array();
while ($r = mysql_fetch_assoc($result)) {
    $rows[] = $r;
}
?>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">ที่อยู่ผู้รับ</h3>
    </div>
    <div class="panel-body">
        <form class="form-signin" id="signup-form" role="form">
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="name" placeholder="ชื่อ" value="<?php echo $rows[0]["name"]; ?>">
                </div>
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="lastname" placeholder="นามสกุล" value="<?php echo $rows[0]["lastname"]; ?>">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="address1" placeholder="ที่อยู่1" value="<?php echo $rows[0]["address1"]; ?>">
                </div>
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="address2" placeholder="ที่อยู่2" value="<?php echo $rows[0]["address2"]; ?>">
                </div>
            </div>
            <br>
            <div class="row">

                <div class="col-xs-6">
                    <input type="text" class="form-control" id="district" placeholder="เขต/อำเภอ" value="<?php echo $rows[0]["district"]; ?>">
                </div>
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="province" placeholder="จังหวัด" value="<?php echo $rows[0]["province"]; ?>">
                </div>
            </div>
            <br>

            <div class="row">

                <div class="col-xs-6">
                    <input type="text" class="form-control" id="zip" placeholder="รหัสไปรษณีย์" value="<?php echo $rows[0]["zip"]; ?>">
                </div>

                <div class="col-xs-6">
                    <input type="text" class="form-control" id="tel" placeholder="เบอร์โทร" value="<?php echo $rows[0]["tel"]; ?>">
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-12">
                    <input type="text" class="form-control" id="note" placeholder="บันทึก">
                </div>
            </div>
        </form>
    </div>
</div>


<?php
$sql = "SELECT SUM(p.weight * c.amount) total_weight FROM product p, cart c WHERE c.member_id = $member_id AND c.status = 0 AND p.id = c.product_id";
//echo $sql;
$result = mysql_query($sql);

$rows = array();
while($r=mysql_fetch_assoc($result)){
    $rows[] = $r;
}
$tw = $rows[0]["total_weight"];
if ($tw < 20)
$ems = 32;
else if ($tw < 100)
$ems = 37;
else if ($tw < 250)
$ems = 42;
else if ($tw < 500)
$ems = 52;
else if ($tw < 1000)
$ems = 67;
else if ($tw < 1500)
$ems = 82;
else if ($tw < 2000)
$ems = 97;
else if ($tw < 2500)
$ems = 112;
else if ($tw < 3000)
$ems = 127;
else if ($tw < 3500)
$ems = 147;
else if ($tw < 4000)
$ems = 167;
else if ($tw < 4500)
$ems = 187;
else if ($tw < 5000)
$ems = 207;
else if ($tw < 5500)
$ems = 232;
else if ($tw < 6000)
$ems = 257;
else if ($tw < 6500)
$ems = 282;
else if ($tw < 7000)
$ems = 307;
else if ($tw < 7500)
$ems = 332;
else if ($tw < 8000)
$ems = 357;
else if ($tw < 8500)
$ems = 387;
else if ($tw < 9000)
$ems = 417;
else if ($tw < 9500)
$ems = 447;
else if ($tw < 10000)
$ems = 477;


if ($tw < 1000)
$normal = 20;
else if ($tw < 2000)
$normal = 35;
else if ($tw < 3000)
$normal = 50;
else if ($tw < 4000)
$normal = 65;
else if ($tw < 5000)
$normal = 80;
else if ($tw < 6000)
$normal =95;
else if ($tw < 7000)
$normal = 110;
else if ($tw < 8000)
$normal = 125;
else if ($tw < 9000)
$normal = 140;
else if ($tw < 10000)
$normal = 155;




?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">ค่าขนส่ง</h3>
    </div>
    <div class="panel-body">
        <div>
            <table class="table table-hover">
                <tbody id="transaction-list">
                    <tr>
                        <th>Option</th>
                        <th>ค่าธรรมเนียม</th>
                        <th>ทั้งหมด</th>
                        <th></th>
                        <input type="hidden" id="delivery" value="0"/>
                        <input type="hidden" id="total" value="0"/>
                    </tr>
                    <tr>
                        <th>EMS</th>
                        <th id="emsfee"><?php echo $ems; ?></th>
                        <th id="emstotal"><?php echo $ems + $total; ?></th>
                        <th><input type="radio" id="radio-ems" name="option" value="1"></th>
                    </tr>
                    <tr>
                        <th>ธรรมดา</th>
                        <th id="normalfee"><?php echo $normal; ?></th>
                        <th id="normaltotal"><?php echo $normal + $total; ?></th>
                        <th><input type="radio" id="radio-normal" name="option" value="2"></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<table class="table">
    <tbody><tr>
        <td><a href="?page=show"><button type="button" class="btn btn-info" id="button-back" style="width: 100%">&lt; กลับไปเลือกสินค้า</button></td>
        <td><button type="button" class="btn btn-success" id="button-pay" style="width: 100%">ยืนยันการสั่งซื้อ &gt;</button></td>
        </tr>
    </tbody></table>

    <!-- ส่งข้อมูลราคาค่าสินค้าทั้งหมดไปยังหน้าชำระเงิน -->
    <script type="text/javascript">
    $("#radio-ems").click(function() {
        $("#total").val($("#emstotal").text());
        $("#delivery").val("ems");
    });

    $("#radio-normal").click(function() {
        $("#total").val($("#normaltotal").text());
        $("#delivery").val("normal");
    });

    $("#button-pay").click(function () {
        var member_id = <?php echo $member_id; ?>;
        var name = $("#name").val();
        var lastname = $("#lastname").val();
        var address1 = $("#address1").val();
        var address2 = $("#address2").val();
        var district = $("#district").val();
        var province = $("#province").val();
        var zip = $("#zip").val();
        var tel = $("#tel").val();
        var note = $("#note").val();
        var total = $("#total").val();
        var delivery = $("#delivery").val();

        if (total == 0) {
            alert("กรุณาเลือกวิธีการจัดส่ง")
        }
        else {
            $.ajax({
    			url: 'db.php',
    			type: "POST",
    			data: {
                    "function": "add_sale",
                    "member_id" : member_id,
                    "name" : name,
                    "lastname" : lastname,
                    "address1" : address1,
                    "address2" : address2,
                    "district" : district,
                    "province" : province,
                    "zip" : zip,
                    "tel" : tel,
                    "note" : note,
                    "total" : total,
                    "delivery" : delivery
    			}
    		}).done(function(id) {
                // console.log(results);
                window.location = "?page=sale&id=" + id;
    		});
        }
    });


    </script>
