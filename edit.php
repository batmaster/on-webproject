<!--สมาชิก-->
<div class="row">
    <div class="row">
        <div class="col-xs-3">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">เมนู</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <ul class="nav nav-pills nav-stacked panel panel-default">
                                    <li role="presentation" class="active"><a href="?page=profile">ข้อมูลส่วนตัว</a></li>
                                    <li role="presentation"><a href="?page=ordered">ข้อมูลการซื้อสินค้า</a></li>
                                    <li role="presentation"><a href="?page=payment">แจ้งชำระเงิน</a></li>
                                </ul>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-xs-9">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">ข้อมูลส่วนตัว</h3>
                </div>
                <div class="panel-body">
                    <?php

                    $username = $_COOKIE["username_member"];
                    $sql = "SELECT * FROM member WHERE email = '$username'";
                    $result = mysql_query($sql);

                    while($r = mysql_fetch_assoc($result)) {
                        $id = $r["id"];
                        $name = $r["name"];
                        $lastname = $r["lastname"];
                        $email = $r["email"];
                        $password = $r["password"];
                        $address1 = $r["address1"];
                        $address2=$r["address2"];
                        $district=$r["district"];
                        $province=$r["province"];
                        $zip=$r["zip"];
                        $tel=$r["tel"];

                        echo "
                        <div class>
                        <div>รหัสสมาชิก: <div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$id\" id=\"id\"disabled></div></div>
                        <div>ชื่อ<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$name\" id=\"name\"></div></div>
                        <div>นามสกุล<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$lastname\" id=\"lastname\"></div></div>
                        <div>อีเมล์<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$email\" id=\"email\"disabled></div></div>
                        <div>รหัสผ่าน<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$password\" id=\"password\"></div></div>
                        <div>ที่อยู่1<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$address1\" id=\"address1\"></div></div>
                        <div>ที่อยู่2<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$address2\" id=\"address2\"></div></div>
                        <div>เขต/อำเภอ<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$district\" id=\"district\"></div></div>
                        <div>จังหวัด<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$province\" id=\"province\"></div></div>
                        <div>เบอร์โทร<div class=\"form-group\"><input type=\"text\" class=\"form-control\" maxlength=\"10\" value=\"$tel\" id=\"tel\"></div></div>
                        <div>รหัสไปรษณีย์<div class=\"form-group\"><input type=\"text\" class=\"form-control\" maxlength=\"5\" value=\"$zip\" id=\"zip\"></div></div>
                        </div>
                        ";
                    }

                    ?>
                    <br>
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-success" id="button-save" style="width: 100%">บันทึก</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-danger" id="button-cancel" style="width: 100%">ยกเลิก</button></div>
                        </div>
                    </div></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        // ปุ่ม id="button-submit"
        $("#button-save").click(function() {
            var id = $("#id").val();
            var name = $("#name").val();
            var lastname = $("#lastname").val();
            var password = $("#password").val();
            var address1 = $("#address1").val();
            var address2 = $("#address2").val();
            var district = $("#district").val();
            var province = $("#province").val();
            var zip = $("#zip").val();
            var tel = $("#tel").val();


            $.ajax({
                url: 'db.php',
                type: "POST",
                // dataType: "json",
                data: {
                    "function":"edit_member",
                    "id":id,
                    "name": name,
                    "lastname":lastname,
                    "password":password,
                    "address1":address1,
                    "address2":address2,
                    "district":district,
                    "province":province,
                    "zip":zip,
                    "tel":tel,

                }
            }).done(function(response) {
                // console.log(response)
                window.location = "?page=profile"
            });
        });

        $("#button-cancel").click(function() {
            location.reload();

        });
        </script>
