<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=member">สมาชิกทั้งหมด</a></li>
                    <li role="presentation" class="active"><a href="?page=ordered">ข้อมูลการซื้อสินค้า</a></li>

                </ul>
            </div>
        </div>

    </div>
        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">ข้อมูลการซื้อสินค้า</h3>
                </div>
                <div class="panel-body">

                    <div>
                        <table class="table table-striped table-bordered" id="id">
                            <tr>
                                <th>#</th>
                                <th>รหัสใบสั่งซื้อ</th>
                                <th>วันที่</th>
                                <th>จำนวนชนิดสินค้า</th>
                                <th>ราคารวม</th>
                                <th>บันทึก</th>
                                <th>สถานะ</th>
                            </tr>
                            <?php
                            $member_id = $_GET["id"];

                            $sql = "SELECT s.id, s.date, (SELECT COUNT(*) FROM sale_item si WHERE si.sale_id = s.id) amount, s.total, s.note, s.tracking, s.status FROM sale s WHERE s.member_id = $member_id";
                            $result = mysql_query($sql);

                            $number = 1;
                            while ($r=mysql_fetch_assoc($result)) {
                                $id = $r["id"];
                                $date = $r["date"];
                                $amount = $r["amount"];
                                $total = $r["total"];
                                $note = $r["note"];
                                $tracking = $r["tracking"];
                                $status = $r["status"] == 0 ? "รอการชำระเงิน" : ($r["status"] == 1 ? "ตรวจสอบการชำระเงิน" : ($r["status"] == 2 ? "รอการส่งของ" : "ส่งของ [$tracking]"));

                                echo "
                                <tr>
                                    <th>$number</th>
                                    <td><a href=\"?page=sale&id=$id\">$id</a></td>
                                    <td>$date</td>
                                    <td>$amount</td>
                                    <td>$total</td>
                                    <td>$note</td>
                                    <td>$status</td>
                                </tr>
                                ";
                                $number++;
                            }

                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
