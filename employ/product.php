<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"class="active"><a href="?page=product">ข้อมูลสินค้า</a></li>
                    <li role="presentation"><a href="?page=add_product">เพิ่มสินค้า</a></li>
                    <li role="presentation"><a href="?page=re_product">รายงานสินค้า</a></li>
                    <li role="presentation"><a href="?page=store">สั่งซื้อสินค้าเข้าร้าน</a></li>
                </ul>
            </div>
        </div>
    </div>


<!--สินค้า-->
<div class="col-xs-9">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">ข้อมูลสินค้า</h3>
        </div>
        <div class="panel-body">
    <?php

    if (isset($_GET["brand"]) && isset($_GET["text"])) {
        $brand = $_GET["brand"];
        if ($brand == "all") {
            $brand = "";
        }
        $text = $_GET["text"];
        $sql = "SELECT * FROM product p WHERE p.brand LIKE '%$brand%' AND name LIKE '%$text%'";
    }
    else {
        $sql = "SELECT * FROM product";
    }
    $result = mysql_query($sql);

    while($r = mysql_fetch_assoc($result)) {
        $id = $r["id"];
        $image = $r["image"];
        $name = $r["brand"] ." " .$r["name"];
        $color = $r["color"];
        $price = $r["price"];


        if ($amount == 0)
            $disable = "disabled";
        else
            $disable = "";

        echo "
        <div class=\"thumbnail col-xs-3\" style=\"padding: 6px; margin: 10px; margin-left: 30px; margin-right: 30px; text-align: center;\">
            <a href=\"?page=re_pro&id=$id\">
                <img src=\"$image\" style=\"width: 150px; height: 150px\">
                <div style=\"margin: 5px;\">$name</div>
            </a>
            <div>$color</div>
            <div>฿$price</div>

            <button type=\"submit\" class=\"btn btn-warning\" onClick=\"location.href='?page=edit_product&id=$id'\">แก้ไข</button>

        </div>


        ";


    }

    ?>

</div>
<center><button type="submit" class="btn btn-success" $disable onClick="location.href='?page=add_product'">เพิ่มสินค้า</button></center><br>
</div>


<!-- Confirm Clar Cart -->
<div class="modal fade" id="remove-product-confirm">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Clear Cart</h4>
			</div>
			<div class="modal-body">
				<p>ลบแน่นะ? มีปัญหานะ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-danger" id="button-confirm-remove-product">Remove</button>
			</div>
		</div>
	</div>
</div>

<script>
function removeProduct(id) {
    $("#remove-product-confirm").modal({
	    show: true
	});
	$("#button-confirm-remove-product").focus();

	$("#button-confirm-remove-product").click(function() {
		$.ajax({
			url: 'db.php',
			type: "POST",
			data: {
                "function": "remove_product",
                "id": id
			}
		}).done(function(results) {
            console.log(results);
            $("#remove-product-confirm").modal('hide');
            // location.reload();
		});
	});
}
</script>
