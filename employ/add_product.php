<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=product">ข้อมูลสินค้า</a></li>
                    <li role="presentation"class="active"><a href="?page=add_product">เพิ่มสินค้า</a></li>
                    <li role="presentation"><a href="?page=re_product">รายงานสินค้า</a></li>
                    <li role="presentation"><a href="?page=store">สั่งซื้อสินค้าเข้าร้าน</a></li>
                </div>
            </div>
        </div>
        <!--เพิ่มสินค้า-->

        <!--สินค้า-->
        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่มสินค้า</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <center>
                                <img id="image-preview" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=200&h=200" width="200" height="200">
                                <input type="file" accept="image/*" id="image" class="btn btn-success navbar-btn" style="width: 90%" />
                            </center>
                        </div>

                        <div class="col-xs-8">
                            <div class="dropdown">
                                <div class="input-group">
                                    <span class="input-group-addon">ยี่ห้อสินค้า</span>
                                    <div class="btn-group" style="width: 100%">

                                <button style="width: 100%" class="btn btn-default dropdown-toggle"type="button" id="dropdownProduct" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-brand="0">
                                    เลือกยี่ห้อสินค้า<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownProduct" style="width: 100%" id="dropdown-product">
                                    <li><a href="#">IPhone</a></li>
                                    <li><a href="#">Samsung</a></li>
                                    <li><a href="#">AIS LAVA</a></li>
                                    <li><a href="#">Zenfone</a></li>
                                    <li><a href="#">Vivo</a></li>
                                    <li><a href="#">OPPO</a></li>
                                    <li><a href="#">HTC</a></li>
                                    <li><a href="#">Wiko</a></li>
                                    <li><a href="#">Acer</a></li>
                                    <li><a href="#">Huawei</a></li>
                                    <li><a href="#">Sony</a></li>
                                    <li><a href="#">Lenovo</a></li>
                                    <li><a href="#">LG</a></li>
                                    <li><a href="#">i-mobile</a></li>
                                    <li><a href="#">อื่นๆ</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                            <br>





                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">ชื่อสินค้า</span>
                            <input type="text" class="form-control" id="input-name">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">สี</span>
                            <input type="text" class="form-control" id="input-color">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">ราคาทุน</span>
                            <input type="text" class="form-control" id="input-cost">
                            <span class="input-group-addon">บาท</span>
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">ราคาขาย</span>
                            <input type="text" class="form-control" id="input-price">
                            <span class="input-group-addon">บาท</span>
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">น้ำหนักสินค้า</span>
                            <input type="text" class="form-control" id="input-weight">

                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">รายละเอียดอื่นๆ</span>
                            <input type="text" class="form-control" id="input-note">

                        </div>
                        <br>






                        <button type="button" class="btn btn-success navbar-btn" id="button-submit">บันทึก</button>
                        <button type="button" class="btn btn-danger navbar-btn"  id="button-cancel">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
    // ปุ่ม id="button-submit"
    // $("#brand-dropdown li").click(function() {
    //     $("#dropdown text").text($(this).text());
    // });


    $("#dropdown-product li a").click(function(e) {
      var selText = $(this).text();
      $('#dropdownProduct').html(selText+'<span class="caret"></span>');
      $('#dropdownProduct').attr("data-brand", selText);
      e.preventDefault();
    });

    $("#button-submit").click(function() {
        var brand = $('#dropdownProduct').attr("data-brand");
        if (brand == "0") {
            alert("กรุณาเลือกยี่ห้อ")
            return;
        }
        var name = $("#input-name").val();
        var color = $("#input-color").val();
        var cost = $("#input-cost").val();
        var price = $("#input-price").val();
        var weight = $("#input-weight").val();
        var note = $("#input-note").val();

        $.ajax({
            url: 'db.php',
            type: "POST",
            // dataType: "json",
            data: {
                "function": "add_product",
                "brand":brand,
                "name": name,
                "color":color,
                "cost":cost,
                "price":price,
                "weight":weight,
                "note":note,
                "image": $("#image-preview").attr('src')
            }
        }).done(function(response) {
            // console.log(response)
            location.reload();
        });


    });

    function readURL(input) {
        for(var i =0; i< input.files.length; i++){
            if (input.files[i]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#image-preview").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    $("#image").change(function(){
        readURL(this);
    });

    $("#button-cancel").click(function() {
        var id = $("#input-id").val();
        alert(id);
        location.reload();

    });
    </script>
