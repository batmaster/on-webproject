<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=product">ข้อมูลสินค้า</a></li>
                    <li role="presentation"><a href="?page=add_product">เพิ่มสินค้า</a></li>
                    <li role="presentation"class="active"><a href="?page=re_product">รายงานสินค้า</a></li>
                    <li role="presentation"><a href="?page=store">สั่งซื้อสินค้าเข้าร้าน</a></li>

                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">รายงานสินค้า</h3>
            </div>
            <div class="panel-body">
                <?php
                require '../db_connection.php';

                $sql = "SELECT * FROM product";
                $result = mysql_query($sql);
                ?>

                <table class="table table-bordered">
                    <thead>
                        <tr bgcolor="#ffff99">
                            <th>#</th>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>ราคา</th>

                            <th>สี</th>
                            <th>จำนวน</th>
                            <th>หมายเหตุ</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $number = 0;
                        while($r = mysql_fetch_assoc($result)) {
                            $number += 1;
                            $id = $r["id"];
                            $name = $r["name"];
                            $price = $r["price"];
                            $color = $r["color"];
                            $amount= $r["amount"];

                            ?>
                            <tr>
                                <th scope="row"><?php echo $number; ?></th>
                                <td><?php echo $id; ?></td>
                                <td><?php echo $name; ?></td>
                                <td><?php echo $price; ?></td>
                                <td><?php echo $color; ?></td>
                                <td><?php echo $amount?></td>
                                <td><a href="?page=re_pro&id=<? echo $id;?>"><button type="button" class="btn btn-info">ดูข้อมูลเพิ่มเติม</button></a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
