<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=member">สมาชิกทั้งหมด</a></li>
                    <li role="presentation" class="active"><a href="?page=ordered">ข้อมูลการซื้อสินค้า</a></li>

                </ul>
            </div>
        </div>

    </div>


    <?php
    $sale_id = $_GET["id"];
    $member_id = $_COOKIE["id_member"];

    $sql = "SELECT * FROM sale s WHERE s.id = $sale_id AND s.member_id = $member_id";
    $result = mysql_query($sql);

    $rows = array();
    while ($r = mysql_fetch_assoc($result)) {
        $rows[] = $r;
    }

    ?>



    <div class="col-xs-9">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">สรุปการสั่งซื้อ</h3>
            </div>
            <div class="panel-body">
                <div>
                    <p><h4>วันที่สั่งซื้อ: </h4><?php echo $rows[0]["date"]; ?></p>
                    <p><h4>ประเภทการส่ง: </h4><?php echo $rows[0]["delivery"] == "ems" ? "EMS" : "ธรรมดา"; ?></p>
                    <table class="table table-striped table-bordered" id="id">
                        <tr>
                            <th>#</th>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>ราคาต่อหน่วย</th>
                            <th>จำนวน</th>
                            <th>ราคารวม</th>
                        </tr>
                        <?php

                        $member_id = $_COOKIE["id_member"];

                        $sql = "SELECT si.product_id, p.name, si.unit_price, si.amount, si.unit_price * si.amount unit_total FROM sale_item si, product p WHERE si.sale_id = $sale_id AND si.product_id = p.id";
                        //echo $sql;
                        $result = mysql_query($sql);

                        $i = 0;
                        $total = 0;
                        while ($r = mysql_fetch_assoc($result)) {
                            $i++;
                            $id = $r["product_id"];
                            $name = $r["name"];
                            $price = $r["unit_price"];
                            $amount = $r["amount"];
                            $unit_total = $r["unit_total"];
                            $total += $unit_total;

                            ?>
                            <tr>
                                <th><?php echo $i; ?></th>
                                <td><?php echo $id; ?></td>
                                <td><?php echo $name; ?></td>
                                <td><?php echo $price; ?></td>
                                <td><?php echo $amount; ?></td>
                                <td><?php echo $unit_total; ?></td>
                            </tr>
                            <?php
                        }
                        echo "<tr><th colspan=5>รวม</th>";
                        echo "<th>$total</th></tr>";
                        ?>
                    </table>
                    <p><h4>จำนวนเงินรวมค่าส่ง: </h4><?php echo $rows[0]["total"]; ?></p>
                </div>
            </div>
        </div>
        <br>

        <?php


        ?>

        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">ที่อยู่ผู้รับ</h3>
            </div>
            <div class="panel-body">
                <form class="form-signin" id="signup-form" role="form">
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="name" placeholder="ชื่อ" value="<?php echo $rows[0]["name"]; ?>" disabled>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="lastname" placeholder="นามสกุล" value="<?php echo $rows[0]["lastname"]; ?>" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="address1" placeholder="ที่อยู่1" value="<?php echo $rows[0]["address1"]; ?>" disabled>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="address2" placeholder="ที่อยู่2" value="<?php echo $rows[0]["address2"]; ?>" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="row">

                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="district" placeholder="เขต/อำเภอ" value="<?php echo $rows[0]["district"]; ?>" disabled>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="province" placeholder="จังหวัด" value="<?php echo $rows[0]["province"]; ?>" disabled>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="zip" placeholder="รหัสไปรษณีย์" value="<?php echo $rows[0]["zip"]; ?>" disabled>
                        </div>

                        <div class="col-xs-6">
                            <input type="text" class="form-control" id="tel" placeholder="เบอร์โทร" value="<?php echo $rows[0]["tel"]; ?>" disabled>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xs-12">
                            <input type="text" class="form-control" id="note" placeholder="บันทึก" disabled>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<!-- ส่งข้อมูลราคาค่าสินค้าทั้งหมดไปยังหน้าชำระเงิน -->
<script type="text/javascript">
$("#radio-ems").click(function() {
    $("#total").val($("#emstotal").text());
    $("#delivery").val("ems");
});

$("#radio-normal").click(function() {
    $("#total").val($("#normaltotal").text());
    $("#delivery").val("normal");
});

$("#button-pay").click(function () {
    var member_id = <?php echo $member_id; ?>;
    var name = $("#name").val();
    var lastname = $("#lastname").val();
    var address1 = $("#address1").val();
    var address2 = $("#address2").val();
    var district = $("#district").val();
    var province = $("#province").val();
    var zip = $("#zip").val();
    var tel = $("#tel").val();
    var note = $("#note").val();
    var total = $("#total").val();
    var delivery = $("#delivery").val();

    if (total == 0) {
        alert("กรุณาเลือกวิธีการจัดส่ง")
    }
    else {
        $.ajax({
            url: 'db.php',
            type: "POST",
            data: {
                "function": "add_sale",
                "member_id" : member_id,
                "name" : name,
                "lastname" : lastname,
                "address1" : address1,
                "address2" : address2,
                "district" : district,
                "province" : province,
                "zip" : zip,
                "tel" : tel,
                "note" : note,
                "total" : total,
                "delivery" : delivery
            }
        }).done(function(results) {
            console.log(results);
        });
    }
});


</script>
