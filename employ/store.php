<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=product">ข้อมูลสินค้า</a></li>
                    <li role="presentation"><a href="?page=add_product">เพิ่มสินค้า</a></li>
                    <li role="presentation"><a href="?page=re_product">รายงานสินค้า</a></li>
                    <li role="presentation"class="active"><a href="?page=store">สั่งซื้อสินค้าเข้าร้าน</a></li>

                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">สั่งสินค้าเข้าร้าน</h3>
            </div>
            <div class="panel-body">


                <div class="row">
                    <div class="col-xs-8">
                        <div class="input-group">
                            <span class="input-group-addon">สินค้า</span>
                            <div class="btn-group" style="width: 100%">
                              <button class="btn btn-default dropdown-toggle" style="width: 100%" id="dropdownProduct" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-id="0">
                                เลือกสินค้า
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownProduct" style="width: 100%">
                                  <?php
                                  $sql = "SELECT p.id, CONCAT(p.id, ' ', p.brand, ' ', p.name) name FROM product p";
                                  $result = mysql_query($sql);

                                  while($r = mysql_fetch_assoc($result)) {
                                      $id = $r["id"];
                                      $name = $r["name"];

                                      echo "<li><a href=\"#\" data-id=\"$id\">$name</a></li>";
                                      }
                                  ?>
                              </ul>
                            </div>
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">จำนวนปัจจุบัน</span>
                            <input type="text" class="form-control" id="amount" value="0" disabled>
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">จำนวนสั่งเพิ่ม</span>
                            <input type="text" class="form-control" id="import-amount" value="0">
                        </div>
                        <br>
                        <button type="button" class="btn btn-success" id="button-add">เพิ่มสินค้าใหม่</button></center></td>
                    </div>
                </div>

                <table class="table table-bordered">
                    <thead>
                        <tr bgcolor="#ffff99">
                            <th>#</th>
                            <th>รหัสสินค้า</th>
                            <th>ชื่อสินค้า</th>
                            <th>จำนวนปัจจุบัน</th>
                            <th>จำนวนสั่งเพิ่ม</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="table-body">

                    </tbody>

                </table>

                    <div class="input-group">
                        <span class="input-group-addon">บันทึกเพิ่มเติม</span>
                        <input type="text" class="form-control" id="note">
                    </div>
                    <br>
                    <center><button type="button" class="btn btn-success" id="button-import">สั่ง</button>
                </div>
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function(){
        $('.dropdown-toggle').dropdown();

        getImportLine();
    });

    $(".dropdown-menu li a").click(function(e) {
      var product_id = $(this).attr("data-id");
      var selText = $(this).text();
      $(this).parents('.btn-group').find('#dropdownProduct').html(selText+'<span class="caret"></span>');
      $(this).parents('.btn-group').find('#dropdownProduct').attr("data-id", product_id);
      e.preventDefault();

      $.ajax({
          url: 'db.php',
          type: "POST",
          dataType: "json",
          data: {
              "function": "get_product",
              "id": product_id
          }
      }).done(function(res) {
          console.log(res)
          $("#amount").val(res[0].amount);
          $("#import-amount").val(0);
      });

    });

    $("#button-add").click(function() {
        var employee_id = <?php echo $_COOKIE["id_employ"]; ?>;
        var product_id = $("#dropdownProduct").attr("data-id");
        var amount = $("#import-amount").val();
        if (product_id != 0 && amount != 0) {
            $.ajax({
                url: 'db.php',
                type: "POST",
                data: {
                    "function": "add_import_line",
                    "employee_id": employee_id,
                    "product_id": product_id,
                    "amount": amount
                }
            }).done(function(res) {
                console.log(res);
                location.reload();
            });

        }
    });

    function getImportLine() {
        var employee_id = <?php echo $_COOKIE["id_employ"]; ?>;

        $.ajax({
            url: 'db.php',
            type: "POST",
            dataType: "json",
            data: {
                "function": "get_import_line",
                "employee_id": employee_id,
            }
        }).done(function(res) {
            console.log(res);

            $("#table-body > tr").remove();
            for (var i = 0; i < res.length; i++) {
                $("#table-body").append("<tr><th>" + (i + 1) + "</th><th>" + res[i]["product_id"] + "</th><th>" + res[i]["name"] + "</th><th>" + res[i]["amount"] + "</th><th>" + res[i]["import_amount"] + "</th><th></th></tr>");
            }
        });
    }

    $("#button-import").click(function() {
        var employee_id = <?php echo $_COOKIE["id_employ"]; ?>;
        var note = $("#note").val();

        $.ajax({
            url: 'db.php',
            type: "POST",
            // dataType: "json",
            data: {
                "function": "add_import",
                "employee_id": employee_id,
                "note": note
            }
        }).done(function(res) {
            console.log(res);
            location.reload();
        });
    });
    </script>
