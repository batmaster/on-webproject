
<!--แก้ไขสินค้า-->
<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"class="active"><a href="?page=product">ข้อมูลสินค้า</a></li>
                    <li role="presentation"><a href="?page=add_product">เพิ่มสินค้า</a></li>
                    <li role="presentation"><a href="?page=re_product">รายงานสินค้า</a></li>
                    <li role="presentation"><a href="?page=store">สั่งซื้อสินค้าเข้าร้าน</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">แก้ไขข้อมูลสินค้า</h3>
            </div>
            <div class="panel-body">
                <?php
                require '../db_connection.php';

                $id = $_GET["id"];
                $sql = "SELECT * FROM product WHERE id = '$id'";
                $result = mysql_query($sql);

                while($r = mysql_fetch_assoc($result)) {
                    $id = $r["id"];
                    $brand = $r["brand"];
                    $name = $r["name"];
                    $color=$r["color"];
                    $cost = $r["cost"];
                    $price=$r["price"];
                    $date=$r["date"];
                    $weight=$r["weight"];
                    $note=$r["note"];
                    $amount=$r["amount"];

                    echo "
                    <div>
                    <div>ยี่ห้อสินค้า<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$brand\"id=\"brand\"disabled></div></div>
                    <div>ชื่อสินค้า<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$name\" id=\"name\"disabled></div></div>
                    <div>สี<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$color\"id=\"color\"disabled></div></div>
                    <div>ราคาทุน<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$cost\"id=\"cost\"disabled></div></div>
                    <div>ราคาขาย<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$price\"id=\"price\"></div></div>
                    <div>วันที่เพิ่มสินค้า<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$date\"id=\"date\"disabled></div></div>
                    <div>น้ำหนักสินค้า<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$weight\"id=\"weight\"disabled></div></div>
                    <div>รายละเอียดเพิ่มเติม<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$note\"id=\"note\"disabled></div></div>
                    <div>จำนวน<div class=\"form-group\"><input type=\"text\" class=\"form-control\" value=\"$amount\"id=\"amount\"disabled></div></div>
                    </div>
                    ";
                }

                ?>
                <br>

                <div class="row">
                    <div class="col-xs-6">
                        <button type="button" class="btn btn-success" id="button-save" style="width: 100%">บันทึก</button>
                    </div>

                    <div class="col-xs-6">
                        <button type="button" class="btn btn-danger" id="button-cancel" style="width: 100%">ยกเลิก</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
// ปุ่ม id="button-submit"
$("#button-save").click(function() {
    var id = <?php echo $_GET["id"]; ?>;
    var price = $("#price").val();


    // alert(+name+lastname+password+password+address+tel);
    /*
    $.ajax({
    url: 'db.php',
    type: "POST",
    dataType: "json",
    data: {
    "id": id
}
}).done(function(response) {

});
*/
// console.log(1);

$.ajax({
    url: 'db.php',
    type: "POST",
    // dataType: "json",
    data: {
        "function": "edit_product",
        "id":id,
        "price":price,
    }
}).done(function(response) {
    console.log(response)
    window.location = "?page=product"
});

// else {
//     alert("รหัสไม่ตรงกัน")
// }

});

$("#button-cancel").click(function() {
    location.reload();

});
</script>
