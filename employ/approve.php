<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=transaction">รายการขายทั้งหมด</a></li>
                    <li role="presentation"class="active"><a href="?page=approve">ตรวจสอบการชำระเงิน</a></li>
                    <li role="presentation"><a href="?page=delivery">ส่งของ</a></li>

                </ul>
            </div>
        </div>
    </div>


    <!--สมาชิก-->
    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">รายการขายทั้งหมด</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="id">
                    <tr>
                        <th>#</th>
                        <th>รหัสการชำระเงิน</th>
                        <th>วันที่</th>
                        <th>ชื่อผู้ชำระ</th>
                        <th>บัญชีโอนเข้า</th>
                        <th>จำนวน</th>
                        <th>ยืนยัน</th>
                    </tr>
                    <?php
                    $member_id = $_GET["id"];

                    $sql = "SELECT DISTINCT(p.id), p.datetime, m.id member_id, CONCAT(m.id, ' ', m.name) name, p.account, p.amount FROM sale s, payment_line pl, payment p, member m WHERE s.id = pl.sale_id AND pl.payment_id = p.id AND p.member_id = m.id AND s.status = 1";
                    $result = mysql_query($sql);

                    $number = 1;
                    while ($r=mysql_fetch_assoc($result)) {
                        $id = $r["id"];
                        $date = $r["datetime"];
                        $member_id = $r["member_id"];
                        $name = $r["name"];
                        $account = $r["account"];
                        $amount = $r["amount"];

                        echo "
                        <tr>
                            <th>$number</th>
                            <td>$id</td>
                            <td>$date</td>
                            <td><a href=\"?page=re_mem&id=$member_id\">$name</a></td>
                            <td>$account</td>
                            <td>$amount</td>
                            <td><button type=\"button\" class=\"btn btn-success\" onclick=\"approve($id)\">ยืนยัน</button></td>
                        </tr>
                        ";
                        $number++;
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>

<script>

function approve(id) {
    $.ajax({
        url: 'db.php',
        type: "POST",
        // dataType: "json",
        data: {
            "function": "approve_payment",
            "id":id,
        }
    }).done(function(response) {
        // console.log(response)
        location.reload();
    });
}

</script>
