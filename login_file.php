<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">login</h3>
    </div>
    <div class="panel-body">
        <form class="form-inline">
            <div class="form-group">
                <label class="sr-only">Email address</label>
                <input type="email" class="form-control" id="email" placeholder="Email">
            </div>
            <br><br>
            <div class="form-group">
                <label class="sr-only">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Password">
            </div>
            <br>
            <br>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember me
                </label>
            </div>
            <br>
            <button type="button" class="btn btn-success navbar-btn" id="login-button">เข้าสู่ระบบ</button>
            <button type="button" class="btn btn-danger navbar-btn">ยกเลิก</button>
            <br>
            <a href="?page=register_mem"><h4 align="center">สมัครสมาชิก</h4></a>
        </form>


    </div>
</div>

<script>
    $("#login-button").click(function() {
        $.ajax({
            url: 'db.php',
            type: "POST",
            data: {
                "function": "login",
                "username": $("#email").val(),
                "password": $("#password").val()
            }
        }).done(function(response) {
            if (response == "0") {
                alert("กรุณาลองอีกรอบ");
            }
            location.reload();
        });
    });
</script>
