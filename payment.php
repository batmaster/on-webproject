<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <ul class="nav nav-pills nav-stacked panel panel-default">
                                <li role="presentation"><a href="?page=profile">ข้อมูลส่วนตัว</a></li>
                                <li role="presentation"><a href="?page=ordered">ข้อมูลการซื้อสินค้า</a></li>
                                <li role="presentation" class="active"><a href="?page=payment">แจ้งชำระเงิน</a></li>
                            </ul>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="col-xs-9">
<div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">แจ้งชำระเงิน</h3>
        </div>
        <div class="panel-body">

            <div class>
                <div class="input-group">
                    <span class="input-group-addon">วันที่ชำระเงิน</span>
                    <input type="text" class="form-control" id="datetime" placeholder="yyyy-mm-dd hh:mm">
                </div>
                <br>

                <div class="input-group">
                    <span class="input-group-addon">จำนวนเงิน</span>
                    <input type="text" class="form-control" id="amount">
                    <span class="input-group-addon">บาท</span>
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">บันทึก</span>
                    <input type="text" class="form-control" id="note">
                </div>
                <br>
                    <h2>บัญชีที่โอนเข้า</h2>
                    <input type="radio" name="account" value="1234-5678-890" checked> ธนาคารกสิกรไทย 1234-5678-890

                <h2>เลขที่ใบสั่งซื้อ</h2>
                <table class="table table-striped table-bordered" id="id">
                    <tr>
                        <th>#</th>
                        <th>รหัสใบสั่งซื้อ</th>
                        <th>วันที่</th>
                        <th>จำนวนชนิดสินค้า</th>
                        <th>ราคารวม</th>
                        <th>บันทึก</th>
                        <th>เลือก</th>
                    </tr>
                    <?php
                        $member_id = $_COOKIE["id_member"];

                        $sql = "SELECT s.id, s.date, (SELECT COUNT(*) FROM sale_item si WHERE si.sale_id = s.id) amount, s.total, s.note, s.status FROM sale s WHERE s.member_id = $member_id AND status = 0";
                        $result = mysql_query($sql);

                        $number = 1;
                        while ($r=mysql_fetch_assoc($result)) {
                            $id = $r["id"];
                            $date = $r["date"];
                            $amount = $r["amount"];
                            $total = $r["total"];
                            $note = $r["note"];
                            $status = $r["status"];

                            echo "
                            <tr>
                                <th>$number</th>
                                <td>$id</td>
                                <td>$date</td>
                                <td>$amount</td>
                                <td>$total</td>
                                <td>$note</td>
                                <td><input type=\"checkbox\" value=\"$id\"></td>
                            </tr>
                            ";
                            $number++;
                        }
                    ?>
                </table>

            </div>
            <br>
            <h4>*กรุณาตรวจทานรายละเอียดให้ถูกต้องอีกครั้ง ก่อนยืนยันการแจ้งชำระเงิน </h4>
            <button type="button" class="btn btn-success" id="button-pay" style="width: 100%">แจ้งชำระเงิน</button>



        </div>



    </div>
</div>
</div>


<script type="text/javascript">

    $("#button-pay").click(function() {
        var member_id = <?php echo $member_id; ?>;
        var datetime = $("#datetime").val();
        var amount = $("#amount").val();
        var note = $("#note").val();
        var account = $("input[type='radio'][name='account']:checked").val();

        var sale_ids = [];
        $(':checkbox:checked').each(function(i) {
          sale_ids[i] = $(this).val();
        });
        console.log(JSON.stringify(sale_ids));

        if (sale_ids.length == 0) {
            alert("กรุณาเลือกเลขที่ใบสั่งของ");
            return;
        }


        $.ajax({
            url: 'db.php',
            type: "POST",
            // dataType: "json",
            data: {
                "function": "add_payment",
                "member_id": member_id,
                "datetime": datetime,
                "amount": amount,
                "note": note,
                "account": account,
                "sale_ids": sale_ids

            }
        }).done(function(response) {
            // console.log(response)
            window.location = "?page=ordered"
        });

    });
</script>
