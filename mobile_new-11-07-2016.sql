-- phpMyAdmin SQL Dump
-- version 2.10.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jul 10, 2016 at 04:42 PM
-- Server version: 5.0.45
-- PHP Version: 5.2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `mobile_new`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `cart`
-- 

CREATE TABLE `cart` (
  `id` int(7) NOT NULL auto_increment,
  `member_id` int(7) NOT NULL,
  `product_id` int(7) NOT NULL,
  `amount` int(7) NOT NULL,
  `status` int(7) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cart`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `employee`
-- 

CREATE TABLE `employee` (
  `id` int(7) NOT NULL auto_increment,
  `email` varchar(32) collate utf8_unicode_ci NOT NULL,
  `password` varchar(32) collate utf8_unicode_ci NOT NULL,
  `name` varchar(32) collate utf8_unicode_ci NOT NULL,
  `lastname` varchar(32) collate utf8_unicode_ci NOT NULL,
  `address` varchar(128) collate utf8_unicode_ci NOT NULL,
  `id_zen` varchar(13) collate utf8_unicode_ci NOT NULL,
  `dob` varchar(32) collate utf8_unicode_ci NOT NULL,
  `gender` varchar(5) collate utf8_unicode_ci NOT NULL,
  `salary` double NOT NULL,
  `tel` varchar(32) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `employee`
-- 

INSERT INTO `employee` VALUES (1, 'e', 'e', 'bat employee', 'bat employee', 'no', 'no', 'no', 'no', 0, 'no');
INSERT INTO `employee` VALUES (2, 's', 'ss', 'a', 's', 's', '12', '2', '', 34, '3');

-- --------------------------------------------------------

-- 
-- Table structure for table `import`
-- 

CREATE TABLE `import` (
  `id` int(7) NOT NULL auto_increment,
  `employee_id` int(7) NOT NULL,
  `date` varchar(20) collate utf8_unicode_ci NOT NULL,
  `note` varchar(128) collate utf8_unicode_ci NOT NULL,
  `submit` int(1) NOT NULL,
  `approve` int(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `import`
-- 

INSERT INTO `import` VALUES (1, 1, '2016-07-09 00:21:23', 'สสส', 1, 0);
INSERT INTO `import` VALUES (2, 1, '2016-07-09 00:21:23', 'สสส', 1, 0);
INSERT INTO `import` VALUES (3, 1, '2016-07-09 00:22:29', 'test', 1, 1);
INSERT INTO `import` VALUES (4, 1, '2016-07-09 01:58:39', '', 1, 0);
INSERT INTO `import` VALUES (5, 1, '2016-07-09 01:59:49', '', 1, 0);
INSERT INTO `import` VALUES (6, 1, '2016-07-09 21:32:26', '', 1, 0);
INSERT INTO `import` VALUES (7, 1, '2016-07-09 23:30:34', '', 1, 0);
INSERT INTO `import` VALUES (8, 1, '2016-07-09 23:31:26', '', 1, 0);
INSERT INTO `import` VALUES (9, 1, '2016-07-10 01:45:00', 'a 99', 1, 1);
INSERT INTO `import` VALUES (10, 1, '2016-07-10 02:06:16', '', 1, 1);
INSERT INTO `import` VALUES (11, 1, '2016-07-10 02:19:36', '', 1, 1);
INSERT INTO `import` VALUES (12, 1, '2016-07-10 02:54:58', '', 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `import_line`
-- 

CREATE TABLE `import_line` (
  `id` int(7) NOT NULL auto_increment,
  `import_id` int(7) NOT NULL,
  `product_id` int(7) NOT NULL,
  `import_amount` int(7) NOT NULL,
  `approve_amount` int(7) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `import_line`
-- 

INSERT INTO `import_line` VALUES (1, 1, 1, 190, 0);
INSERT INTO `import_line` VALUES (2, 1, 2, 70, 0);
INSERT INTO `import_line` VALUES (3, 1, 4, 200, 0);
INSERT INTO `import_line` VALUES (4, 2, 1, 500, 0);
INSERT INTO `import_line` VALUES (5, 2, 3, 10, 0);
INSERT INTO `import_line` VALUES (6, 3, 2, 20, 0);
INSERT INTO `import_line` VALUES (7, 4, 3, 5, 0);
INSERT INTO `import_line` VALUES (8, 5, 3, 5, 0);
INSERT INTO `import_line` VALUES (9, 6, 2, 7, 0);
INSERT INTO `import_line` VALUES (10, 7, 2, 20, 0);
INSERT INTO `import_line` VALUES (11, 8, 1, 10, 0);
INSERT INTO `import_line` VALUES (12, 8, 2, 1, 0);
INSERT INTO `import_line` VALUES (13, 9, 1, 99, 90);
INSERT INTO `import_line` VALUES (14, 10, 1, 200, 200);
INSERT INTO `import_line` VALUES (15, 11, 14, 500, 450);
INSERT INTO `import_line` VALUES (16, 12, 4, 6, 0);
INSERT INTO `import_line` VALUES (17, 12, 14, 2, 0);
INSERT INTO `import_line` VALUES (18, 12, 1, 2, 2);

-- --------------------------------------------------------

-- 
-- Table structure for table `member`
-- 

CREATE TABLE `member` (
  `id` int(7) NOT NULL auto_increment,
  `email` varchar(32) collate utf8_unicode_ci NOT NULL,
  `password` varchar(32) collate utf8_unicode_ci NOT NULL,
  `name` varchar(32) collate utf8_unicode_ci NOT NULL,
  `lastname` varchar(32) collate utf8_unicode_ci NOT NULL,
  `address1` varchar(128) collate utf8_unicode_ci NOT NULL,
  `address2` varchar(128) collate utf8_unicode_ci NOT NULL,
  `district` varchar(32) collate utf8_unicode_ci NOT NULL,
  `province` varchar(32) collate utf8_unicode_ci NOT NULL,
  `zip` varchar(5) collate utf8_unicode_ci NOT NULL,
  `tel` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `member`
-- 

INSERT INTO `member` VALUES (2, 'b', 'b', 'ปรเมศวร์', 'หอมประกอบ', '9/2 ม.7 ต.เสือหึง', '', 'เชียรใหญ่', 'นครศรีธรรมราช', '80190', '0817371393');

-- --------------------------------------------------------

-- 
-- Table structure for table `owner`
-- 

CREATE TABLE `owner` (
  `id` int(7) NOT NULL auto_increment,
  `email` varchar(32) collate utf8_unicode_ci NOT NULL,
  `password` varchar(32) collate utf8_unicode_ci NOT NULL,
  `name` varchar(32) collate utf8_unicode_ci NOT NULL,
  `lastname` varchar(32) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `owner`
-- 

INSERT INTO `owner` VALUES (1, 'ad', 'ad', 'admin', 'admin');
INSERT INTO `owner` VALUES (2, 'a', 'a', 'aa', 'aa');

-- --------------------------------------------------------

-- 
-- Table structure for table `payment`
-- 

CREATE TABLE `payment` (
  `id` int(7) NOT NULL,
  `date` varchar(20) collate utf8_unicode_ci NOT NULL,
  `address1` varchar(128) collate utf8_unicode_ci NOT NULL,
  `address2` varchar(128) collate utf8_unicode_ci NOT NULL,
  `district` varchar(32) collate utf8_unicode_ci NOT NULL,
  `province` varchar(32) collate utf8_unicode_ci NOT NULL,
  `zip` varchar(5) collate utf8_unicode_ci NOT NULL,
  `tel` varchar(10) collate utf8_unicode_ci NOT NULL,
  `note` varchar(128) collate utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `account` varchar(32) collate utf8_unicode_ci NOT NULL,
  `datetime` varchar(20) collate utf8_unicode_ci NOT NULL,
  `image` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table `payment`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `payment_line`
-- 

CREATE TABLE `payment_line` (
  `id` int(7) NOT NULL,
  `payment_id` int(7) NOT NULL,
  `sale_id` int(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Dumping data for table `payment_line`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `product`
-- 

CREATE TABLE `product` (
  `id` int(7) NOT NULL auto_increment,
  `brand` varchar(32) collate utf8_unicode_ci NOT NULL,
  `name` varchar(32) collate utf8_unicode_ci NOT NULL,
  `color` varchar(32) collate utf8_unicode_ci NOT NULL,
  `cost` double NOT NULL,
  `price` double NOT NULL,
  `date` varchar(20) collate utf8_unicode_ci NOT NULL,
  `weight` double NOT NULL,
  `note` varchar(128) collate utf8_unicode_ci NOT NULL,
  `amount` int(7) NOT NULL,
  `image` blob NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `product`
-- 

INSERT INTO `product` VALUES (1, 'a', 'a', 'a', 10, 1000, 'a', 10, 'a', 302, '');
INSERT INTO `product` VALUES (2, '', 's', 's', 34, 1200, '3', 4, 'note', 0, '');
INSERT INTO `product` VALUES (3, '', 'e', 'e', 4, 50, '3', 2, 'n', 0, '');
INSERT INTO `product` VALUES (4, '', 'g', 'g', 5, 670, '6', 5, 'g', 0, '');
INSERT INTO `product` VALUES (14, '', 'aa', 'a', 1, 1, 'a', 0, 'a', 450, '');

-- --------------------------------------------------------

-- 
-- Table structure for table `sale`
-- 

CREATE TABLE `sale` (
  `id` int(7) NOT NULL auto_increment,
  `member_id` int(7) NOT NULL,
  `status` int(1) NOT NULL,
  `tracking` varchar(32) collate utf8_unicode_ci NOT NULL,
  `date` varchar(20) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `sale`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `sale_item`
-- 

CREATE TABLE `sale_item` (
  `id` int(7) NOT NULL auto_increment,
  `sale_id` int(7) NOT NULL,
  `product_id` int(7) NOT NULL,
  `amount` int(7) NOT NULL,
  `unit_price` double NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `sale_item`
-- 

