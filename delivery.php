<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title"align="center">อัตราค่าขนส่ง</h3>
    </div>
    <div class="panel-body">
        <table border="1" align="center">

            <tr><th colspan="3"><h3 align="center">อัตราค่าขนส่งสินค้าแบบพัสดุธรรมดา</h3></th></tr>
            <tr>

                <th><h4 align="center">น้ำหนัก(Kg.)</h4></th>
                <th><h4 align="center">ราคา(บาท)</h4></th>
                <th rowspan="14"> <img width="450" height="200" src="images/download.jpg"></th>
            </tr>
                <tr>
                    <td><h5 align="center">0 - 1</h5></td>
                    <td><h5 align="center">20</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">1.01 - 2</h5> </td>
                    <td><h5 align="center">35</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">2.01 - 3</h5></td>
                    <td><h5 align="center">50</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">3.01 - 4 </h5></td>
                    <td><h5 align="center">65</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">4.01 - 5</h5>  </td>
                    <td><h5 align="center">80</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">5.01 - 6</h5> </td>
                    <td><h5 align="center">95</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">6.01 - 7</h5>  </td>
                    <td><h5 align="center">110</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">7.01 - 8</h5> </td>
                    <td><h5 align="center">125</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">8.01 - 9</h5> </td>
                    <td><h5 align="center">140</h5></td>
                </tr>
                <tr>
                    <td><h5 align="center">9.01 - 10</h5> </td>
                    <td><h5 align="center">155</h5></td>
                </tr>
            </table>

            <br>
            <br>

            <table border="1" align="center">
                    <tr><th colspan="3"><h3 align="center">อัตราค่าขนส่งสินค้าแบบพัสดุด่วน (EMS)</h3></th></tr>
                <tr>
                    <th><h4 align="center">น้ำหนัก(Kg.)</h4></th>
                    <th><h4 align="center">ราคา(บาท)</h4></th>
                    <th rowspan="25"> <img width="450" height="200" src="images/nd.jpg"></th>

                </tr>
                    <tr>
                        <td><h5 align="center">0-20</h5></td>
                        <td><h5 align="center">32</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">21-100</h5></td>
                        <td><h5 align="center">37</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">101-250</h5></td>
                        <td><h5 align="center">42</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">251-500</h5></td>
                        <td><h5 align="center">52</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">501-1000</h5></td>
                        <td><h5 align="center">67</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">1001-1500</h5></td>
                        <td><h5 align="center">82</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">1501-2000</h5></td>
                        <td><h5 align="center">97</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">2001-2500</h5></td>
                        <td><h5 align="center">112</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">2501-3000</h5></td>
                        <td><h5 align="center">127</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">3001-3500</h5></td>
                        <td><h5 align="center">147</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">3501-4000</h5></td>
                        <td><h5 align="center">167</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">4001-4500</h5></td>
                        <td><h5 align="center">187</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">4501-5000</h5> </td>
                        <td><h5 align="center">207</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">5001-5500</h5> </td>
                        <td><h5 align="center">232</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">5501-6000</h5></td>
                        <td><h5 align="center">257</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">6001-6500</h5></td>
                        <td><h5 align="center">282</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">6501-7000</h5></td>
                        <td><h5 align="center">307</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">7001-7500</h5></td>
                        <td><h5 align="center">332</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">7501-8000</h5></td>
                        <td><h5 align="center">257</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">8001-8500</h5></td>
                        <td><h5 align="center">387</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">8501-9000</h5></td>
                        <td><h5 align="center">417</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">9001-9500</h5></td>
                        <td><h5 align="center">447</h5></td>
                    </tr>
                    <tr>
                        <td><h5 align="center">9501-10000</h5></td>
                        <td><h5 align="center">477</h5></td>
                    </tr>


            </table>


    </div>
</div>
