<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"class="active"><a href="?page=transaction">รายการขายทั้งหมด</a></li>
                    <li role="presentation"><a href="?page=approve">ตรวจสอบการชำระเงิน</a></li>
                    <li role="presentation"><a href="?page=delivery">ส่งของ</a></li>
                    <li class="nav-divider"></li>
                    <li role="presentation"><a href="?page=report_import">รายงานการสั่งรายสินค้า</a></li>
                    <li role="presentation"><a href="?page=report">รายงานการขายรายสินค้า</a></li>
                </ul>
            </div>
        </div>
    </div>


    <!--สมาชิก-->
    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">รายการขายทั้งหมด</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="id">
                    <tr>
                        <th>#</th>
                        <th>รหัสใบสั่งซื้อ</th>
                        <th>วันที่</th>
                        <th>ชื่อผู้ซื้อ</th>
                        <th>ราคารวม</th>
                        <th>บันทึก</th>
                        <th>สถานะ</th>
                    </tr>
                    <?php
                    $member_id = $_GET["id"];

                    $sql = "SELECT s.id, s.date, m.id member_id, CONCAT(m.id, ' ', m.name) name, s.total, s.note, s.tracking, s.status FROM sale s, member m WHERE s.member_id = m.id";
                    $result = mysql_query($sql);

                    $number = 1;
                    while ($r=mysql_fetch_assoc($result)) {
                        $id = $r["id"];
                        $date = $r["date"];
                        $member_id = $r["member_id"];
                        $name = $r["name"];
                        $total = $r["total"];
                        $note = $r["note"];
                        $tracking = $r["tracking"];
                        $status = $r["status"] == 0 ? "รอการชำระเงิน" : ($r["status"] == 1 ? "ตรวจสอบการชำระเงิน" : ($r["status"] == 2 ? "รอการส่งของ" : "ส่งของ [$tracking]"));

                        echo "
                        <tr>
                            <th>$number</th>
                            <td><a href=\"?page=sale&id=$id\">$id</a></td>
                            <td>$date</td>
                            <td><a href=\"?page=mem&id=$member_id\">$name</a></td>
                            <td>$total</td>
                            <td>$note</td>
                            <td>$status</td>
                        </tr>
                        ";
                        $number++;
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>
