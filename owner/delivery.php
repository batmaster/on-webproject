<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=transaction">รายการขายทั้งหมด</a></li>
                    <li role="presentation"><a href="?page=approve">ตรวจสอบการชำระเงิน</a></li>
                    <li role="presentation"class="active"><a href="?page=delivery">ส่งของ</a></li>
                    <li class="nav-divider"></li>
                    <li role="presentation"><a href="?page=report_import">รายงานการสั่งรายสินค้า</a></li>
                    <li role="presentation"><a href="?page=report">รายงานการขายรายสินค้า</a></li>
                </ul>
            </div>
        </div>
    </div>


    <!--สมาชิก-->
    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">ส่งของ</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered" id="id">
                    <tr>
                        <th>#</th>
                        <th>รหัสการสั่งซื้อ</th>
                        <th>ชื่อ ที่อยู่</th>
                        <th>ประเภทการส่ง</th>
                    </tr>
                    <?php
                    $member_id = $_GET["id"];

                    $sql = "SELECT * FROM sale s WHERE s.status = 2";
                    $result = mysql_query($sql);

                    $number = 1;
                    while ($r=mysql_fetch_assoc($result)) {
                        $id = $r["id"];
                        $name = $r["name"] . " " .$r["name"]. "<br>" .$r["address1"] ." " .$r["address2"] ." " .$r["district"] ." " .$r["province"] ." " .$r["zip"] ." " .$r["tel"];
                        $delivery = $r["delivery"];

                        echo "
                        <tr>
                            <th>$number</th>
                            <td><a href=\"?page=sale&id=$id\">$id</a></td>
                            <td>$name</td>
                            <td>$delivery</td>
                        </tr>
                        ";
                        $number++;
                    }

                    ?>
                </table>
            </div>
        </div>
    </div>

<script>

function delivery(id) {
    alert(id);

    $("input[type='text']").each(function(i) {
        if ($(this).attr('data-id') == id) {
            var code = $(this).val();

            $.ajax({
                url: 'db.php',
                type: "POST",
                // dataType: "json",
                data: {
                    "function": "delivery",
                    "id": id,
                    "code": code
                }
            }).done(function(response) {
                // console.log(response)
                location.reload();
            });
        }
    });
}

</script>
