<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=transaction">รายการขายทั้งหมด</a></li>
                    <li role="presentation"><a href="?page=approve">ตรวจสอบการชำระเงิน</a></li>
                    <li role="presentation"><a href="?page=delivery">ส่งของ</a></li>
                    <li class="nav-divider"></li>
                    <li role="presentation"class="active"><a href="?page=report_import">รายงานการสั่งรายสินค้า</a></li>
                    <li role="presentation"><a href="?page=report">รายงานการขายรายสินค้า</a></li>
                </ul>
            </div>
        </div>
    </div>


    <!--สมาชิก-->
    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">รายการขายทั้งหมด</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                  <div class='col-xs-6'>
                    <div class="form-group">
                      <div class='input-group date' id='datetimepicker6' style="width: 100%">
                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">เริ่มวันที่</span>
                          <input type='text' class="form-control" id="from" placeholder="yyyy-mm-dd hh:mm:ss" data-date-format='YYYY-MM-DD HH:mm:ss'>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class='col-xs-6'>
                    <div class="form-group">
                      <div class='input-group date' id='datetimepicker7' style="width: 100%">
                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">ถึงวันที่</span>
                          <input type='text' class="form-control" id="to" placeholder="yyyy-mm-dd hh:mm:ss"  data-date-format='YYYY-MM-DD HH:mm:ss'>
                          <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon">สินค้า</span>
                            <div class="btn-group" style="width: 100%">
                                <?php
                                $data_id = 0;
                                $text = "ทั้งหมด";
                                if (isset($_GET["id"])) {
                                    $data_id = $_GET["id"];
                                    if ($data_id != "0") {
                                        $sql = "SELECT CONCAT(p.id, ' ', p.brand, ' ', p.name) id FROM product p WHERE p.id = $data_id";
                                        $result = mysql_query($sql);

                                        $r = mysql_fetch_assoc($result);
                                        $text= $r["id"];
                                    }
                                }
                                ?>
                              <button class="btn btn-default dropdown-toggle" style="width: 100%" id="dropdownProduct" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-id="​<?php echo $data_id; ?>">
                                ​<?php echo $text; ?>
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownProduct" style="width: 100%">
                                  <li><a href="#" data-id="0">ทั้งหมด</a></li>
                                  <?php
                                  $sql = "SELECT p.id, CONCAT(p.id, ' ', p.brand, ' ', p.name) name FROM product p";
                                  $result = mysql_query($sql);

                                  while($r = mysql_fetch_assoc($result)) {
                                      $id = $r["id"];
                                      $name = $r["name"];

                                      echo "<li><a href=\"#\" data-id=\"$id\">$name</a></li>";
                                      }
                                  ?>
                              </ul>
                            </div>
                        </div>
                        <br>
                        <button type="button" class="btn btn-success" id="button-search">ตรวจสอบ</button></center></td>
                    </div>
                </div>
                <br>
                <br>

                <table class="table table-striped table-bordered" id="id">
                    <tr>
                        <th>#</th>
                        <th>รหัสสินค้า</th>
                        <th>รหัสใบสั่งของ</th>
                        <th>วันที่</th>
                        <th>ชื่อพนักงาน</th>
                        <th>จำนวนสั่ง</th>
                        <th>จำนวนอนุมัติ</th>
                        <th>บันทึก</th>
                    </tr>
                    <?php

                    if (isset($_GET["id"])) {
                        $id = $_GET["id"];
                        if ($id == "0") {
                            $id = "";
                        }
                        else {
                            $id = " AND p.id = $id";
                        }
                        $from = $_GET["from"];
                        $to = $_GET["to"];

                        $sql = "SELECT il.product_id, CONCAT(il.product_id, ' ', p.brand, ' ', p.name) product, i.id, i.date, CONCAT(e.id, ' ', e.name) name, il.import_amount, il.approve_amount, i.note FROM import i, import_line il, employee e, product p WHERE '$from' <= i.date AND i.date <= '$to' $id AND i.id = il.import_id AND i.employee_id = e.id AND p.id = il.product_id AND i.approve = 1 ORDER BY i.date";
                    }
                    else {
                        $sql = "SELECT il.product_id, CONCAT(il.product_id, ' ', p.brand, ' ', p.name) product, i.id, i.date, CONCAT(e.id, ' ', e.name) name, il.import_amount, il.approve_amount, i.note FROM import i, import_line il, employee e, product p WHERE i.id = il.import_id AND i.employee_id = e.id AND p.id = il.product_id AND i.approve = 1 ORDER BY i.date";
                    }

                    $result = mysql_query($sql);

                    $number = 1;
                    while ($r=mysql_fetch_assoc($result)) {
                        $product_id = $r["product_id"];
                        $product = $r["product"];
                        $id = $r["id"];
                        $date = $r["date"];
                        $name = $r["name"];
                        $import_amount = $r["import_amount"];
                        $approve_amount = $r["approve_amount"];
                        $note = $r["note"];

                        echo "
                        <tr>
                            <th>$number</th>
                            <td><a href=\"?page=product&id=$product_id\">$product</a></td>
                            <td><a href=\"?page=approve_line&id=$id\">$id</a></td>
                            <td>$date</td>
                            <td>$name</td>
                            <td>$import_amount</td>
                            <td>$approve_amount</td>
                            <td>$note</td>
                        </tr>
                        ";
                        $number++;
                    }
                    
                    ?>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.dropdown-toggle').dropdown();
        });

        $(".dropdown-menu li a").click(function(e) {
          var product_id = $(this).attr("data-id");
          var selText = $(this).text();
          $(this).parents('.btn-group').find('#dropdownProduct').html(selText+'<span class="caret"></span>');
          $(this).parents('.btn-group').find('#dropdownProduct').attr("data-id", product_id);
          e.preventDefault();

          $.ajax({
              url: 'db.php',
              type: "POST",
              dataType: "json",
              data: {
                  "function": "get_product",
                  "id": product_id
              }
          }).done(function(res) {
              console.log(res)
              $("#amount").val(res[0].amount);
              $("#import-amount").val(0);
          });

        });

        $("#button-search").click(function() {
            var param = "";

            var id = $('#dropdownProduct').attr("data-id").replace(/\u200B/g,'');
            param += "&id=" + id;

            var from = $('#from').val();
            param += "&from=" + from;

            var to = $('#to').val();
            param += "&to=" + to;

            window.location = "?page=report_import" + param;
        });

        <?php
        $sql = "SELECT i.date FROM import i ORDER BY i.id LIMIT 1";
        $result = mysql_query($sql);

        $r = mysql_fetch_assoc($result);
        $from = $r["date"];


        $sql = "SELECT NOW() now";
        $result = mysql_query($sql);

        $r = mysql_fetch_assoc($result);
        $to = $r["now"];
        ?>

        $(function () {
            $('#datetimepicker6').datetimepicker();
            $('#datetimepicker7').datetimepicker({
                useCurrent: false //Important! See issue #1075
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });

            $("#from").val("<?php if (isset($_GET["from"])) echo $_GET["from"]; else echo $from; ?>");
            $("#to").val("<?php if (isset($_GET["to"])) echo $_GET["to"]; else echo $to; ?>");
        });
    </script>
