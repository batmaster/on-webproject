<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"class="active"><a href="?page=re_product">รายงานสินค้าทั้งหมด</a></li>
                    <li role="presentation"><a href="?page=approve_import">อนุมัติใบสั่งซื้อเข้าร้าน</a></li>

                </div>
            </div>
        </div>

        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">รายงานสินค้าทั้งหมด</h3>
                </div>
                <div class="panel-body">
                    <?php
                    if (isset($_GET["brand"]) && isset($_GET["text"])) {
                        $brand = $_GET["brand"];
                        if ($brand == "all") {
                            $brand = "";
                        }
                        $text = $_GET["text"];
                        $sql = "SELECT * FROM product p WHERE p.brand LIKE '%$brand%' AND name LIKE '%$text%'";
                    }
                    else {
                        $sql = "SELECT * FROM product";
                    }
                    $result = mysql_query($sql);
                    ?>

                    <table class="table table-bordered">
                        <thead>
                            <tr bgcolor="#99ff33">
                                <th>#</th>
                                <th>รหัสสินค้า</th>
                                <th>ยี่ห้อ</th>
                                <th>ชื่อสินค้า</th>
                                <th>สี</th>
                                <th>ราคา</th>
                                <th>จำนวน</th>
                                <th>การจัดการ</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $number = 0;
                            while($r = mysql_fetch_assoc($result)) {
                                $number += 1;
                                $id = $r["id"];
                                $brand = $r["brand"];
                                $name = $r["name"];
                                $color = $r["color"];
                                $price = $r["price"];
                                $amount = $r["amount"];



                                ?>
                                <tr>
                                    <th scope="row"><?php echo $number; ?></th>
                                    <td><?php echo $id; ?></td>
                                    <td><?php echo $brand; ?></td>
                                    <td><?php echo $name; ?></td>
                                    <td><?php echo $color; ?></td>
                                    <td><?php echo $price; ?></td>
                                    <td><?php echo $amount; ?></td>

                                    <td><a href="?page=product&id=<? echo $id;?>"><button type="button" class="btn btn-info">ดูข้อมูลเพิ่มเติม</button></a></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
