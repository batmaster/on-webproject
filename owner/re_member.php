<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"class="active"><a href="?page=re_member">รายงานสมาชิกทั้งหมด</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เนื้อหา</h3>
            </div>
            <div class="panel-body">
                <?php

                $sql = "SELECT * FROM member";
                $result = mysql_query($sql);
                ?>

                <table class="table table-bordered">
                    <thead>
                        <tr bgcolor="#99ff33">
                            <th>#</th>
                            <th>รหัสสมาชิก</th>
                            <th>ชื่อสมาชิก</th>
                            <th>นามสกุล</th>
                            <th>เบอร์โทร</th>
                            <th>สถานะ</th>
                            <th>การจัดการ</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $number = 0;
                        while($r = mysql_fetch_assoc($result)) {
                            $number += 1;
                            $id = $r["id"];
                            $name = $r["name"];
                            $lastname = $r["lastname"];
                            $tel = $r["tel"];



                            ?>
                            <tr>
                                <th scope="row"><?php echo $number; ?></th>
                                <td><?php echo $id; ?></td>
                                <td><?php echo $name; ?></td>
                                <td><?php echo $lastname; ?></td>
                                <td><?php echo $tel; ?></td>
                                <td> </td>
                                <td><a href="?page=mem&id=<?php echo $id; ?>"><button type="button" class="btn btn-info">ดูข้อมูลเพิ่มเติม</button></a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
