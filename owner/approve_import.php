<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=re_product">รายงานสินค้าทั้งหมด</a></li>
                    <li role="presentation"class="active"><a href="?page=approve_import">อนุมัติใบสั่งซื้อเข้าร้าน</a></li>

                </div>
            </div>
        </div>

        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">อนุมัติใบสั่งซื้อเข้าร้าน</h3>
                </div>
                <div class="panel-body">

                    <table class="table table-bordered">
                        <thead>
                            <tr bgcolor="#99ff33">
                                <th>#</th>
                                <th>รหัสใบสั่งซื้อ</th>
                                <th>ชื่อพนักงาน</th>
                                <th>วันที่</th>
                                <th>บันทึกเพิ่มเติม</th>
                                <th>สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            $sql = "SELECT i.id, e.name, i.date, i.note, i.approve FROM import i, employee e WHERE i.employee_id = e.id ORDER BY i.submit, i.date DESC, i.id DESC";
                            $result = mysql_query($sql);

                            $rows = array();
                            $number = 1;
                            while($r = mysql_fetch_assoc($result)) {
                                $id = $r["id"];
                                $name = $r["name"];
                                $date = $r["date"];
                                $note = $r["note"];
                                $approve = $r["approve"] == 0 ? "ยังไม่อนุมัติ" : "อนุมัติแล้ว";
                                echo "<tr>
                                    <th>$number</th>
                                    <th>$id</th>
                                    <th>$name</th>
                                    <th>$date</th>
                                    <th>$note</th>
                                    <th><a href=\"?page=approve_line&id=$id\">$approve</a></th>
                                </tr>";
                                $number++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
