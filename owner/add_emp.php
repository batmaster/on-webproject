
<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=re_emp">รายงานพนักงานทั้งหมด</a></li>
                        <li role="presentation"class="active"><a href="?page=add_emp">เพิ่มข้อมูลพนักงาน</a></li>
                </ul>
            </div>
        </div>
    </div>
        <!--เพิ่มข้อมูลพนักงาน-->
        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">เนื้อหา</h3>
                </div>
                <div class="panel-body">

                    <div class>


                        <!--ชื่อพนักงาน-->
                        <div class="input-group">
                            <span class="input-group-addon">ชื่อพนักงาน</span>
                            <input type="text" class="form-control" id="input-name">
                        </div>
                        <br>


                        <!--นามสกุล-->
                        <div class="input-group">
                            <span class="input-group-addon">นามสกุล</span>
                            <input type="text" class="form-control" id="input-lastname">
                        </div>
                        <br>
                        <!--รหัสผ่าน-->
                        <div class="input-group">
                            <span class="input-group-addon">รหัสผ่าน</span>
                            <input type="text" class="form-control" id="input-password">
                        </div>
                        <br>
                        <div class="input-group">
                            <span class="input-group-addon">อีเมล์</span>
                            <input type="text" class="form-control" id="input-email">
                        </div>
                        <br>
                        <!--ที่อยู่-->
                        <div class="input-group">
                            <span class="input-group-addon">ที่อยู่</span>
                            <input type="text" class="form-control" id="input-address">
                        </div>
                        <br>
                        <!--รหัสประจำตัวบัตรประชาชน-->
                        <div class="input-group">
                            <span class="input-group-addon">รหัสประจำตัวบัตรประชาชน</span>
                            <input type="text" class="form-control" id="input-id_zen">
                        </div>
                        <br>
                        <!--วันเกิด-->
                        <div class="input-group">
                            <span class="input-group-addon">วันเกิด</span>
                            <input type="text" class="form-control" id="input-dob">
                        </div>
                        <br>
                        <!--เพศ-->
                        <div class="input-group">
                            <span class="input-group-addon">เพศ</span>
                            <input type="text" class="form-control" id="input-gender">
                        </div>
                        <br>
                        <!--เงินเดือน-->
                        <div class="input-group">
                            <span class="input-group-addon">เงินเดือน</span>
                            <input type="text" class="form-control" id="input-salary">
                        </div>
                        <br>
                        <!--เบอร์โทร-->
                        <div class="input-group">
                            <span class="input-group-addon">เบอร์โทร</span>
                            <input type="text" class="form-control" id="input-tel">
                        </div>
                        <br>
<center>
                        <button type="button" class="btn btn-success navbar-btn"id="button-submit">บันทึก</button>
                        <button type="button" class="btn btn-danger navbar-btn"id="button-cancel">ยกเลิก</button>
                    </center>
                    </div>
                </div>

            </div>


        </div>
        <script type="text/javascript">
        // ปุ่ม id="button-submit"
        $("#button-submit").click(function() {
            var name = $("#input-name").val();
            var lastname = $("#input-lastname").val();
            var email = $("#input-email").val();
            var password = $("#input-password").val();
            var address = $("#input-address").val();
            var id_zen = $("#input-id_zen").val();
            var dob = $("#input-dob").val();
            var gender = $("#input-gender").val();
            var salary = $("#input-salary").val();
            var tel = $("#input-tel").val();

            // alert(เพิ่มเรียบร้อย);
            /*
            $.ajax({
            url: 'db.php',
            type: "POST",
            dataType: "json",
            data: {
            "id": id
        }
    }).done(function(response) {

});
*/
console.log(1);

$.ajax({
    url: 'db.php',
    type: "POST",
    // dataType: "json",
    data: {
        "function": "add_employee",
        "name": name,
        "lastname":lastname,
        "password":password,
        "email":email,
        "address":address,
        "id_zen":id_zen,
        "dob":dob,
        "gender":gender,
        "salary":salary,
        "tel":tel,


    }
}).done(function(response) {
    // console.log(response)
    window.location = "?page=re_emp"
});

// else {
//     alert("รหัสไม่ตรงกัน")
// }

});

$("#button-cancel").click(function() {
    location.reload();

});
</script>
