<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"class="active"><a href="#">รายงานตัวแทนจำหน่ายทั้งหมด</a></li>
                </div>
            </div>
        </div>

        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">เนื้อหา</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $host="localhost";
                    $user="root"; // MySql Username
                    $pass="12345678"; // MySql Password
                    $dbname="mobile"; // Database Name

                    $conn = mysql_connect($host, $user, $pass) or die("ไม่สามารถเชื่อมต่อฐานข้อมูลได้"); // เชื่อมต่อ ฐานข้อมูล
                    mysql_select_db($dbname, $conn); // เลือกฐานข้อมูล
                    mysql_query("SET NAMES utf8"); // กำหนด charset ให้ฐานข้อมูล เพื่ออ่านภาษาไทย

                    $sql = "SELECT * FROM dealer";
                    $result = mysql_query($sql);
                    ?>

                    <table class="table table-bordered">
                        <thead>
                            <tr bgcolor="#99ff33">
                                <th>#</th>
                                <th>รหัสตัวแทนจำหน่าย</th>
                                <th>ชื่อ</th>
                                <th>ที่อยู่(บริษัทสังกัด)</th>
                                <th>เบอร์โทร</th>
                                <th>พนักงานผู้ติดต่อ</th>
                                <th>สถานะ</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $number = 0;
                            while($r = mysql_fetch_assoc($result)) {
                                $number += 1;
                                $dealer_id = $r["dealer_id"];
                                $dealer_name = $r["dealer_name"];
                                $dealer_company = $r["dealer_company"];
                                $dealer_tel = $r["dealer_tel"];
                                $employer_id = $r["employer_id"];
                                $dealer_status = $r["dealer_status"];

                                ?>
                                <tr>
                                    <th scope="row"><?php echo $number; ?></th>
                                    <td><?php echo $dealer_id; ?></td>
                                    <td><?php echo $dealer_name; ?></td>
                                    <td><?php echo $dealer_company; ?></td>
                                    <td><?php echo $dealer_tel; ?></td>
                                    <td><?php echo $employer_id; ?></td>
                                    <td><?php echo $dealer_status; ?></td>

                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
