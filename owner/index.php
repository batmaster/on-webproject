<?php
    require '../db_connection.php';
?>

<html lang="en">
<head>
    <script src="../js/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datetimepicker.css">
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-dropdown.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <script src="../js/collapse.js"></script>
    <script src="../js/moment.js"></script>
    <script src="../js/transition.js"></script>
    <script src="../js/bootstrap-datepicker.th.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.min.js"></script>

    <meta charset="UTF-8">
    <title>owner - สากลโมบาย.COM</title>
</head>
<body>
    <!--แถบบนสุด-->
    <div class="container">
    <!-- <div><img width="1150" height="200" src="images/es.jpg" border="1" hspace="0"></div> -->
    <nav class="navbar navbar-default">
      <div class="container-fluid">
          <div class="collapse navbar-collapse">
    <h1 align="right"><I>ระบบจัดการร้านสากลโมบาย</I></h1>



        <ul class="nav nav-pills nav-justified">
            <?php
            if (isset($_COOKIE["username_owner"]) && $_COOKIE["username_owner"] != "") {
                echo "<li role=\"presentation\""; if ($_GET["page"] == "user") echo "class=\"active\""; echo "><a href=\"?page=user\">ข้อมูลส่วนตัว</a></li>";
                echo "<li role=\"presentation\""; if ($_GET["page"] == "emp" || $_GET["page"] == "re_emp" || $_GET["page"] == "add_emp") echo "class=\"active\""; echo "><a href=\"?page=add_emp\">พนักงาน</a></li>";
                echo "<li role=\"presentation\""; if ($_GET["page"] == "re_member" || $_GET["page"] == "mem" || $_GET["page"] == "ordered" || $_GET["page"] == "sale") echo "class=\"active\""; echo "><a href=\"?page=re_member\">สมาชิก</a></li>";
                echo "<li role=\"presentation\""; if ($_GET["page"] == "re_product" || $_GET["page"] == "approve_import" || $_GET["page"] == "approve_line" || $_GET["page"] == "product") echo "class=\"active\""; echo "><a href=\"?page=re_product\">สินค้า</a></li>";
                echo "<li role=\"presentation\""; if ($_GET["page"] == "transaction" || $_GET["page"] == "approve" || $_GET["page"] == "delivery" || $_GET["page"] == "report" || $_GET["page"] == "report_import") echo "class=\"active\""; echo "><a href=\"?page=transaction\">รายการขาย</a></li>";
                echo "<li role=\"presentation\"><a href=\"#\" id=\"logout\">ออกจากระบบ</a></li>";
            }
            ?>
        </ul>

    </div>

    </div>
    </nav>

<br>

<div class="container">
    <div class="row">
        <div class="col-xs-2">
            <div class="dropdown">
                <button style="width: 100%" class="btn btn-info dropdown-toggle" type="button" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-brand="<?php if (isset($_GET["brand"]) && $_GET["brand"] != "all") echo $_GET["brand"]; else echo "ทั้งหมด"; ?>">
                    <?php if (isset($_GET["brand"]) && $_GET["brand"] != "all") echo $_GET["brand"]; else echo "ทั้งหมด"; ?><span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownSearch" id="dropdown-search">
                    <li><a href="#">ทั้งหมด</a></li>
                    <li><a href="#">IPhone</a></li>
                    <li><a href="#">Samsung</a></li>
                    <li><a href="#">AIS LAVA</a></li>
                    <li><a href="#">Zenfone</a></li>
                    <li><a href="#">Vivo</a></li>
                    <li><a href="#">OPPO</a></li>
                    <li><a href="#">HTC</a></li>
                    <li><a href="#">Wiko</a></li>
                    <li><a href="#">Acer</a></li>
                    <li><a href="#">Huawei</a></li>
                    <li><a href="#">Sony</a></li>
                    <li><a href="#">Lenovo</a></li>
                    <li><a href="#">LG</a></li>
                    <li><a href="#">i-mobile</a></li>
                    <li><a href="#">อื่นๆ</a></li>
                </ul>
            </div>
            <br>
        </div>

            <div class="col-xs-10">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" id="search-box" value="<?php if (isset($_GET["text"])) echo $_GET["text"]; else echo ""; ?>">
                    <span class="input-group-btn">
                        <button id="search-button" class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                </div>

            </div>




        <br>

        <!--แถบที่2-->

        <div class="row">
            <div class="col-xs-12">
                <?php
                if (isset($_COOKIE["username_owner"]) && $_COOKIE["username_owner"] != "")
                if ($_GET["page"] == "add_emp")
                include"add_emp.php";
                else if ($_GET["page"] == "re_emp")
                include"re_emp.php";
                else if ($_GET["page"] == "re_member")
                include"re_member.php";
                else if ($_GET["page"] == "re_product")
                include"re_product.php";
                else if ($_GET["page"] == "emp")
                include"emp.php";
                else if ($_GET["page"] == "mem")
                include"mem.php";
                else if ($_GET["page"] == "user")
                include"user.php";
                else if ($_GET["page"] == "confirm")
                include"confirm.php";
                else if ($_GET["page"] == "approve_import")
                include "approve_import.php";
                else if ($_GET["page"] == "approve_line")
                include "approve_line.php";
                else if ($_GET["page"] == "ordered")
                include "ordered.php";
                else if ($_GET["page"] == "sale")
                include "sale.php";
                else if ($_GET["page"] == "transaction")
                include "transaction.php";
                else if ($_GET["page"] == "approve")
                include "approve.php";
                else if ($_GET["page"] == "delivery")
                include "delivery.php";
                else if ($_GET["page"] == "report")
                include "report.php";
                else if ($_GET["page"] == "report_import")
                include "report_import.php";
                else if ($_GET["page"] == "product")
                include "product.php";
                else
                include"re_product.php";
                else
                include "login.php";
                ?>



            </div>
        </div>






</body>
<script>
    $("#logout").click(function() {
        $.ajax({
            url: 'db.php',
            type: "POST",
            data: {
                "function": "logout"
            }
        }).done(function(response) {
            location.reload();
        });
    });

    $(document).ready(function(){
        $('.dropdown-toggle').dropdown();
    });

    $("#dropdown-search li a").click(function(e) {
      var selText = $(this).text();
      $('#dropdownSearch').html(selText+'<span class="caret"></span>');
      $('#dropdownSearch').attr("data-brand", selText);
      e.preventDefault();
    });

    $("#search-button").click(function() {
        var brand = $('#dropdownSearch').attr("data-brand");
        if (brand == "ทั้งหมด") {
            brand = "all";
        }
        var text = $("#search-box").val();

        window.location.href = "?page=re_product&brand=" + brand + "&text=" + text;
    });


</script>

</html>
</div>
</div>
