<?php
    if (isset($_POST["approve_amount"])) {
        for ($i = 0; $i < count($_POST["approve_amount"]); $i++) {
            $approve_amount = $_POST["approve_amount"][$i];
            $import_line_id = $_POST["import_line_id"][$i];
            $sql = "UPDATE import_line SET approve_amount = $approve_amount WHERE id = $import_line_id";
            mysql_query($sql);

            $import_id = $_GET["id"];
            $sql = "UPDATE import SET approve = 1 WHERE id = $import_id";
            mysql_query($sql);

            $sql = "UPDATE product SET amount = amount + $approve_amount WHERE id = (SELECT il.product_id FROM import_line il WHERE il.id = $import_line_id)";
            mysql_query($sql);
        }
    }
?>

<div class="row">
    <div class="col-xs-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">เมนู</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked panel panel-default">
                    <li role="presentation"><a href="?page=re_product">รายงานสินค้าทั้งหมด</a></li>
                    <li role="presentation"class="active"><a href="?page=approve">อนุมัติใบสั่งซื้อเข้าร้าน</a></li>

                </div>
            </div>
        </div>

        <div class="col-xs-9">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">ใบสั่งซื้อ</h3>
                </div>
                <div class="panel-body">

                    <?php
                        $import_id = $_GET["id"];
                        $sql = "SELECT i.id, e.name, i.date, i.note, i.approve FROM import i, employee e WHERE i.employee_id = e.id AND i.id = $import_id   ORDER BY i.submit, i.date DESC, i.id DESC";
                        $result = mysql_query($sql);

                        $rows = array();
                        while($r = mysql_fetch_assoc($result)) {
                            $rows[] = $r;
                        }
                        $approved = $rows[0]["approve"];
                    ?>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="input-group">
                                <span class="input-group-addon">รหัสใบสั่งซื้อ</span>
                                <input type="text" class="form-control" value="<?php echo $rows[0]["id"]; ?>" disabled>
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">ชื่อพนักงาน</span>
                                <input type="text" class="form-control" value="<?php echo $rows[0]["name"]; ?>" disabled>
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">วันที่</span>
                                <input type="text" class="form-control" value="<?php echo $rows[0]["date"]; ?>" disabled>
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">บันทึกเพิ่มเติม</span>
                                <input type="text" class="form-control" value="<?php echo $rows[0]["note"]; ?>" disabled>
                            </div>
                        </div>
                    </div>
                    <br>

                    <form method="POST" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <table class="table table-bordered">
                            <thead>
                                <tr bgcolor="#ffff99">
                                    <th>#</th>
                                    <th>รหัสสินค้า</th>
                                    <th>ชื่อสินค้า</th>
                                    <?php
                                        if (!$approved) {
                                            echo "<th>จำนวนปัจจุบัน</th>";
                                        }
                                    ?>
                                    <th>จำนวนสั่งเพิ่ม</th>
                                    <th>จำนวนอนุมัติ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $sql = "SELECT il.id import_line_id, p.id, p.name, p.amount, il.import_amount, il.approve_amount FROM import_line il, product p WHERE il.product_id = p.id AND il.import_id = $import_id";
                                    $result = mysql_query($sql);

                                    $number = 1;
                                    while($r = mysql_fetch_assoc($result)) {
                                        $import_line_id = $r["import_line_id"];
                                        $id = $r["id"];
                                        $name = $r["name"];
                                        $amount = $r["amount"];
                                        $import_amount = $r["import_amount"];
                                        $i = $number - 1;
                                        $approve_amount = $r["approve_amount"];

                                        if (!$approved) {
                                            $amount = "<td>$amount</td>";
                                            $approve = "<td><input type=\"number\" name=\"approve_amount[$i]\" min=\"0\"/><input type=\"hidden\" name=\"import_line_id[$i]\" value=\"$import_line_id\"></td></th>";
                                        }
                                        else {
                                            $amount = "";
                                            $approve = "<td>$approve_amount</td>";
                                        }
                                        echo "<tr><th>$number</th><td>$id</td><td>$name</td>$amount<td>$import_amount</td>$approve</tr>";

                                        $number++;
                                    }
                                ?>
                            </tbody>

                        </table>
                        <br>
                        <?php
                            if (!$approved) {
                                echo "<br><center><button type=\"submit\" class=\"btn btn-success\">อนุมัติ</button>";
                            }
                        ?>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
